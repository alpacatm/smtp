#define _GNU_SOURCE

#include <check.h>
#include "server-parse.h"
#include "server-fsm.h"
#include <time.h>
#include <unistd.h>
#include <sys/queue.h>
#include "stdio.h"
#include <errno.h>
#include <stdlib.h>

START_TEST (check_helo)
{
	ck_assert(parse_cmd("HELO\r\n") == SERVER_FSM_EV_HELO);
	ck_assert(parse_cmd("     HELO\r\n") == SERVER_FSM_EV_HELO);
	ck_assert(parse_cmd("HELO    \n") == SERVER_FSM_EV_HELO);
	ck_assert(parse_cmd("    HELO    \n") == SERVER_FSM_EV_HELO);
}
END_TEST

START_TEST (check_ehlo)
{
	ck_assert(parse_cmd("EHLO\r\n") == SERVER_FSM_EV_EHLO);
	ck_assert(parse_cmd("EHLO\n") == SERVER_FSM_EV_EHLO);
	ck_assert(parse_cmd("EHLO      \r\n") == SERVER_FSM_EV_EHLO);
	ck_assert(parse_cmd("     EHLO\r\n") == SERVER_FSM_EV_EHLO);
	ck_assert(parse_cmd("     EHLO    \r\n") == SERVER_FSM_EV_EHLO);
	ck_assert(parse_cmd("     EHLO  jskdfhkjldsgfsdgf  \r\n") == SERVER_FSM_EV_EHLO);
}
END_TEST

START_TEST (check_quit)
{
	ck_assert(parse_cmd("QUIT\r\n") == SERVER_FSM_EV_QUIT);
	ck_assert(parse_cmd("QUIT\n") == SERVER_FSM_EV_QUIT);
	ck_assert(parse_cmd("  QUIT\n") == SERVER_FSM_EV_QUIT);
	ck_assert(parse_cmd("QUIT   \n") == SERVER_FSM_EV_QUIT);
	ck_assert(parse_cmd("   QUIT   \n") == SERVER_FSM_EV_QUIT);
}
END_TEST

START_TEST (check_rset)
{
	int event = SERVER_FSM_EV_RSET;
	ck_assert(parse_cmd("RSET\r\n") == event);
	ck_assert(parse_cmd("RSET\n") == event);
	ck_assert(parse_cmd("rset\n") == event);
	ck_assert(parse_cmd("    rset\n") == event);
	ck_assert(parse_cmd("rset    \n") == event);
	ck_assert(parse_cmd("      rset    \n") == event);
}
END_TEST

START_TEST (check_mail)
{
	int event = SERVER_FSM_EV_MAIL;
	ck_assert(parse_cmd("MAIL FROM:<@USC-ISIE.ARPA:owl@some.domain>\r\n") == SERVER_FSM_EV_MAIL);
	ck_assert(strcmp(get_param(event, "MAIL FROM:<@USC-ISIE.ARPA:owl@some.domain>\r\n"), "owl@some.domain") == 0);
	ck_assert(parse_cmd("MAIL FROM:<>\r\n") == SERVER_FSM_EV_MAIL);
	ck_assert(strcmp(get_param(event, "MAIL FROM:<>\r\n"), "") == 0);
	ck_assert(parse_cmd("MAIL FROM:\r\n") == SERVER_FSM_EV_MAIL);
	ck_assert(parse_cmd("    MAIL   FROM    :   \r\n") == SERVER_FSM_EV_MAIL);
	ck_assert(parse_cmd("    MAIL   FROM    :   \n") == SERVER_FSM_EV_MAIL);
	ck_assert(get_param(event, "MAIL FROM:\r\n") == NULL);
}
END_TEST

START_TEST (check_rcpt)
{
	int event = SERVER_FSM_EV_RCPT;
	ck_assert(parse_cmd("RCPT TO:<@hosta.int,@jkl.org:userc@d.bar.org>\n") == event);
	ck_assert(parse_cmd("RCPT TO:<@d.d.d.d-d.d.d,@jkl.org:userc@d.bar.org>\n") == event);
	ck_assert(parse_cmd("RCPT TO:<@[127.0.0.1],@jkl.org:userc@d.bar.org>\n") == event);
	ck_assert(parse_cmd("RCPT TO:<@[127.0.0.1] , @jkl.org:userc@d.bar.org>\n") == event);
	ck_assert(strcmp(get_param(event, "RCPT TO:<@hosta.int,@jkl.org:userc@d.bar.org>\n"), "userc@d.bar.org") == 0);
	ck_assert(parse_cmd("RCPT TO:<userc@d.bar.org>\n") == event);
	ck_assert(strcmp(get_param(event, "RCPT TO:<userc@d.bar.org>\n"), "userc@d.bar.org") == 0);
	ck_assert(parse_cmd("RCPT TO:<>\n") == event);
	ck_assert(parse_cmd("  RCPT   TO  :  <  >  \n") == event);
	ck_assert(get_param(event, "RCPT TO:<>\n") == NULL);
}
END_TEST

START_TEST (check_vrfy)
{
	int event = SERVER_FSM_EV_VRFY;
	ck_assert(parse_cmd("VRFY some_str\r\n") == event);
	ck_assert(parse_cmd("VRFY some_str\n") == event);
	ck_assert(parse_cmd("vrfy some_str\n") == event);
	ck_assert(parse_cmd("vrfy   some_str  \n") == event);
	ck_assert(parse_cmd("     vrfy some_str\n") == event);
	ck_assert(parse_cmd("     vrfy    some_str   \n") == event);
}
END_TEST

int main(void) {
    Suite *s1 = suite_create("Client");
    TCase *tc1_1 = tcase_create("Client");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, check_helo);
    tcase_add_test(tc1_1, check_ehlo);
    tcase_add_test(tc1_1, check_quit);
    tcase_add_test(tc1_1, check_mail);
    tcase_add_test(tc1_1, check_rcpt);
    tcase_add_test(tc1_1, check_rset);
    tcase_add_test(tc1_1, check_vrfy);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf != 0;
}
