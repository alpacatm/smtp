#define _GNU_SOURCE

#include <check.h>
#include "client.h"
#include "server-fsm.h"
#include <time.h>
#include <unistd.h>
#include <sys/queue.h>
#include "stdio.h"
#include <errno.h>
#include <stdlib.h>

START_TEST (check_new_client)
{
	Client* client = new_client(1, 1, 1, "");
	ck_assert_int_eq(client->connection.sock, 1);
	ck_assert_int_eq(client->connection.port, 1);
	ck_assert(client->connection.client_ip[0] == 0);
	ck_assert_int_eq(client->state, SERVER_FSM_ST_INIT);
	ck_assert(client->mail != NULL);
	ck_assert(client->mail->from == NULL);
	ck_assert(client->mail->data == NULL);
	ck_assert(client->mail->x_original_to == NULL);
	ck_assert(LIST_EMPTY(&client->mail->recipients));
	ck_assert_int_eq(client->finished, 0);
	ck_assert_int_eq(client->content_finished, 0);
	ck_assert_int_eq(client->my_mail, 0);
	ck_assert((client->last_activity - time(0)) > -2);
	delete_client(&client);
}
END_TEST

START_TEST (check_client_clear_buffers)
{
	Client* client = new_client(1, 1, 1, "");
	client->reply = malloc(10);
	client->mail->from = malloc(10);
	client->mail->data = malloc(10);
	client->mail->x_original_to = malloc(10);
	client_clear_buffers(client);

	ck_assert(client->reply != NULL);
	ck_assert(client->mail->from == NULL);
	ck_assert(client->mail->data == NULL);
	ck_assert(client->mail->x_original_to == NULL);
	delete_client(&client);
}
END_TEST

START_TEST (check_delete_client)
{
	Client* client = new_client(1, 1, 1, "");
	delete_client(&client);
	ck_assert(client == NULL);
}
END_TEST

START_TEST (check_fill_reply)
{
	Client* client = new_client(1, 1, 1, "");
	fill_reply(client, "123");
	ck_assert(strcmp(client->reply, "123") == 0);
	fill_reply(client, "456");
	ck_assert(strcmp(client->reply, "456") == 0);
	delete_client(&client);
}
END_TEST

int main(void) {
    Suite *s1 = suite_create("Client");
    TCase *tc1_1 = tcase_create("Client");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, check_new_client);
    tcase_add_test(tc1_1, check_client_clear_buffers);
    tcase_add_test(tc1_1, check_delete_client);
    tcase_add_test(tc1_1, check_fill_reply);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf != 0;
}
