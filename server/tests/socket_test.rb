#!/usr/bin/ruby

require 'socket'
sock = TCPSocket.new 'localhost', 2525

commands = ["EHLO\n",
			"MAIL FROM:<>\n",
			"RCPT TO:<pro3owl@unknown.rcpt>\n",
			"DATA\n"
			]

puts "[server]: " + sock.gets

for command in commands
  puts "[client]: " + command
  sock.puts command
  puts "[server]: " + (sock.gets || "!!!NO RESULT!!!")
end
puts "[client]: " + "Blah blah blah...\r\n"
sock.puts "Blah blah blah...\r\n"
puts "[client]: " + "...etc. etc. etc.\r\n"
sock.puts "...etc. etc. etc.\r\n"
puts "[client]: " + ".\r\n"
sock.puts ".\r\n"
puts "[server]: " + (sock.gets || "!!!NO RESULT!!!")

puts "[client]: " + "QUIT\r\n"
sock.puts "QUIT\r\n"
puts "[server]: " + (sock.gets || "!!!NO RESULT!!!")

sock.close