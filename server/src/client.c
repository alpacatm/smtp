#include "client.h"

/** \file client.c
 *  \brief Обработка структуры Client.
 *
 *   Создание нового клиента и удаление существующего, сброс накопленных буферов.
 */

#include <stdlib.h>
#include <string.h>

Client* new_client(int id, int client_sock, int port, ip_str_t client_ip)
{
	Client *client = malloc(sizeof(Client));
	client->id = id;

	client->connection.sock = client_sock;
	client->connection.port = port;
	strcpy(client->connection.client_ip, client_ip);

	client->state = SERVER_FSM_ST_INIT;
	client->mail = new_mail();
	client->reply = NULL;
	client->finished = 0;
	client->content_finished = 0;
	client->my_mail = 0;
	client->last_activity = time(0);

	return client;
}

void client_clear_buffers(Client* client)
{
	client->content_finished = 0;
	clear_mail(client->mail);
}


void delete_client(Client** client)
{
	free_mail(&(*client)->mail);
	if (*client) {
		if ((*client)->reply != NULL) {
			free((*client)->reply);
		}
		free(*client);
		*client = NULL;
	}
}

void fill_reply(Client* client, char* const_str) {
	if (client->reply != NULL) {
		free(client->reply);
	}
	client->reply = malloc(sizeof(char) * (strlen(const_str) + 1));
	strcpy(client->reply, const_str);
}
