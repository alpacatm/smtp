/** \file main.c
 *  \brief Запуск приложения.
 *
 *   Инициализация программы: чтение файла конфигурации и заполнение глобальных настроек;
 *   иницализация журналирования.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <libconfig.h>
#include <stdlib.h>

#include "logger.h"
#include "settings.h"
#include "serve.h"

config_t cfg;
extern ServerSettings global_settings;

int get_int_option(config_t cfg, const char* name, int def) {
	int value = def;
	config_lookup_int(&cfg, name, &value);
	return value;
}

const char* get_str_option(config_t cfg, const char* name, const char* def) {
	const char* value = def;
	config_lookup_string(&cfg, name, &value);
	return value;
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		info("usage: %s <config-file>\n", argv[0]);
		return 1;
	}

	config_init(&cfg);
	if (!config_read_file(&cfg, argv[1])) {
		error("error: can't open configuration file %s: error at line %d: '%s'",
		      config_error_file(&cfg),
		      config_error_line(&cfg),
		      config_error_text(&cfg)
		     );
		return 1;
	}

	global_settings.address = get_str_option(cfg, "server.ip", "0.0.0.0");
	global_settings.port = get_int_option(cfg, "server.port", 25);
	global_settings.timeout = get_int_option(cfg, "server.timeout", 10);
	global_settings.max_data_length = get_int_option(cfg, "server.max_data_length", 10485760); /*10 Mb*/
	global_settings.processes_count = get_int_option(cfg, "server.workers", 3);
	global_settings.maildir = get_str_option(cfg, "server.maildir", "");
	global_settings.domain = get_str_option(cfg, "server.domain", "");
	global_settings.start_time = time(0);

	config_setting_t* relay_domains_setting = config_lookup(&cfg, "server.relay_domains");
	global_settings.relay_domains_count = config_setting_length(relay_domains_setting);
	if (global_settings.relay_domains_count > 0) {
		global_settings.relay_domains = malloc(sizeof(char*) * global_settings.relay_domains_count);
		for (int i = 0; i < global_settings.relay_domains_count; i++) {
			global_settings.relay_domains[i] = config_setting_get_string_elem(relay_domains_setting, i);
		}
	} else {
		global_settings.relay_domains = NULL;
	}

	info("my domain: %s", global_settings.domain);
	for (int i = 0; i < global_settings.relay_domains_count; i++) {
		info("relay domain: %s", global_settings.relay_domains[i]);
	}

	const char* log_file = get_str_option(cfg, "server.log", NULL);
	if (log_file != NULL) {
		int retval = init_logger(log_file);
		if (retval) return retval;
	}

	return run();
}
