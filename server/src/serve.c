#include "serve.h"

/** \file serve.c
 *  \brief TCP-сервер.
 *
 *  Запуск рабочих процессов и обработка сетевого ввода-вывода.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <poll.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/prctl.h>
#include <signal.h>
#include <assert.h>

#include "logger.h"
#include "client.h"
#include "perform.h"

int my_worker_id;
extern ServerSettings global_settings;
ClientsList clients;
int server_socket;

const int INPUT_EVENTS_FLAG = POLLIN | POLLPRI;
const int OUTPUT_EVENTS_FLAG = POLLOUT;

/** \fn void insert_client(int client_sock, ClientsList* clients, int* clients_count, ip_str_t client_ip, int port)
 *  \brief Инициализация нового клиента.
 *  \param client_sock -- клиентский сокет
 *  \param client_ip -- ip входящего соединения
 *  \param port -- port входящего соединения
 *  \return Ничего.
 *  Создает структуру для хранения клиента и добавляет её в список обрабатываемых клиентов.
 */
void insert_client(int client_sock, ClientsList* clients, int* clients_count,
                   ip_str_t client_ip, int port) {
	static int i = 1;

	int id = i++ * global_settings.processes_count + my_worker_id + global_settings.start_time;

	Client *client = new_client(id, client_sock, port, client_ip);

	SLIST_INSERT_HEAD(clients, client, next);
	*clients_count = *clients_count + 1;

	initialize(client);
	info("%d new client %s:%d", client->id,
	     client->connection.client_ip, client->connection.port);
}

/** \fn void remove_connection(Client** _client, ClientsList* clients, int* clients_count)
 *  \brief Закрытие сокета и удаление клиента из списка клиентов
 *  \return Ничего.
 *  Если нужно еще что-то записать в буфер (ответ от клиента) - пишет это.
 *  Затем делает shutdown чтения на сокете и закрывает его.
 *  Попутно удаляет клиента из списка клиентов.
 */
void remove_connection(Client** _client, ClientsList* clients, int* clients_count) {
	Client* client = *_client;
	info("%d removing client", client->id);
	if (client) {
		if (client->reply != NULL) {
			send(client->connection.sock, client->reply, strlen(client->reply), MSG_DONTWAIT);
		}
		info("%d client %s:%d disconnected", client->id,
		     client->connection.client_ip, client->connection.port);
		if (shutdown(client->connection.sock, SHUT_RD) < 0) {
			warn("%d error closing connection: %s", client->id, strerror(errno));
		}
		close(client->connection.sock);

		Client* tmp = SLIST_NEXT(client, next);
		SLIST_REMOVE(clients, client, Client, next);
		delete_client(_client);

		*_client = tmp;
		*clients_count = *clients_count - 1;
	}
}

/** \fn void read_stream(Client** _client, ClientsList* clients, int* clients_count)
 *  \brief Чтение из клиентского сокета.
 *  \return Ничего.
 *  Читает из сокета данные.
 *  Вызывает обработку данных с помощью конечного автомата сервера.
 *  В случае, если прочитать ничего не удалось или обработка завершена, инициирует удаление клиента.
 */
void read_stream(Client** _client, ClientsList* clients, int* clients_count) {
	Client* client = *_client;
	char buf[MAXCMDLEN];
	memset(buf, 0, MAXCMDLEN);

	ssize_t size = recv(client->connection.sock, &buf, MAXCMDLEN - 1, 0);
	if (size == 0) {
		remove_connection(_client, clients, clients_count);
		return;
	}

	buf[size] = '\0';
	int retval = perform(client, buf, size);
	if (retval != 0) {
		client->finished = 1;
		remove_connection(_client, clients, clients_count);
	} else {
		*_client = SLIST_NEXT(client, next);
	}
}

/** \fn void write_stream(Client* client, ClientsList* clients, int* clients_count)
 *  \brief Запись ожидающих данных в клиентский сокет.
 *  \return Ничего.
 *  Пишет в сокет последний ответ сервера, если он есть.
 */
void write_stream(Client* client, ClientsList* clients, int* clients_count) {
	if (client->reply != NULL) {
		send(client->connection.sock, client->reply, strlen(client->reply), MSG_DONTWAIT);
		free(client->reply);
		client->reply = NULL;
	}
}

/** \fn void accept_new_client(int server_socket, ClientsList* clients, int* clients_count)
 *  \brief Принятие входящего соединения.
 *  \return Ничего.
 *  Делает accept и если получает клиентский сокет, то добавляет его в список обрабатываемых клиентов.
 */
void accept_new_client(int server_socket, ClientsList* clients, int* clients_count) {
	struct sockaddr client_addr;
	memset(&client_addr, 0, sizeof(struct sockaddr));
	socklen_t addrlen = sizeof(struct sockaddr);

	int client_sock = accept(server_socket, &client_addr, &addrlen);
	if (client_sock <= 0) {
		if (errno != EWOULDBLOCK && errno != EAGAIN) {
			error("accept error: %s", strerror(errno));
		}
	} else {
		struct sockaddr_in* addr_v4 = (struct sockaddr_in*) &client_addr;
		ip_str_t client_ip;
		inet_ntop( AF_INET, &addr_v4->sin_addr.s_addr, client_ip, INET_ADDRSTRLEN );

		insert_client(client_sock, clients, clients_count, client_ip, addr_v4->sin_port);
	}
}

void fill_poll_entry(struct pollfd *ufd, int sock, int flag) {
	ufd->fd = sock;
	ufd->events = flag;
}

/** \fn void fill_ufds(struct pollfd *ufds, int server_socket, ClientsList* clients, int flag)
 *  \brief Заполнение массива дескрипторов для последующего вызова poll.
 *  \return Ничего.
 *  Флаг на ожидание записи ставится только если есть не пустой ответ сервера.
 */
void fill_ufds(struct pollfd *ufds, int server_socket, ClientsList* clients, int flag) {
	fill_poll_entry(ufds, server_socket, flag);
	Client* client = SLIST_FIRST(clients);
	for (int i = 1; client != NULL; i++, client = SLIST_NEXT(client, next)) {
		int cflag = flag;
		if (client->reply != NULL) cflag |= POLLOUT;
		fill_poll_entry(ufds + i, client->connection.sock, cflag);
	}
}

/** \fn void serve()
 *  \brief Основная функция рабочего процесса.
 *  \return Ничего.
 *  В бесконечном цикле делает poll сокетов (в том числе серверного сокета) и
 *  вызывает соответствующие операции чтения/записи/закрытия соединения при срабатывании
 *  соответствующих событий
 */
int serve()
{
	SLIST_INIT(&clients);
	int clients_count = 0;

	int timeout = 1000;
	int ufds_count = 1;
	struct pollfd *ufds = malloc(sizeof(struct pollfd) * ufds_count);

	info("worker started");
	while (1) {
		if (ufds_count != clients_count + 1) {
			ufds_count = clients_count + 1;
			ufds = realloc(ufds, sizeof(struct pollfd) * ufds_count);
		}

		fill_ufds(ufds, server_socket, &clients, INPUT_EVENTS_FLAG);
		/*int ready_count = */poll(ufds, ufds_count, timeout);

		Client* client = SLIST_FIRST(&clients);
		for (int i = 1; i < ufds_count; i++) {
			assert(client != NULL);
			if (ufds[i].revents & POLLHUP) {
				remove_connection(&client, &clients, &clients_count);
			} else if (ufds[i].revents & (POLLIN | POLLPRI)) {
				read_stream(&client, &clients, &clients_count);
			} else if (ufds[i].revents & POLLOUT) {
				write_stream(client, &clients, &clients_count);
				check_timeout(client);
				if (client->finished) {
					remove_connection(&client, &clients, &clients_count);
				} else {
					client = SLIST_NEXT(client, next);
				}
			} else {
				check_timeout(client);
				if (client->finished) {
					remove_connection(&client, &clients, &clients_count);
				} else {
					client = SLIST_NEXT(client, next);
				}
			}
		}

		// accept new connection
		if (ufds[0].revents & (POLLIN | POLLPRI)) {
			accept_new_client(server_socket, &clients, &clients_count);
		}
	}

	return 0;
}

/** \fn void close_clients()
 *  \brief Закрытие всех клиентов при остановке сервера.
 *  \return Ничего.
 */
void close_clients() {
	Client* client = SLIST_FIRST(&clients);
	while(client != NULL) {
		if (shutdown(client->connection.sock, 2) < 0) {
			error("%d error closing connection: %s", client->id, strerror(errno));
		}
		close(client->connection.sock);

		Client* tmp = SLIST_NEXT(client, next);
		SLIST_REMOVE(&clients, client, Client, next);
		delete_client(&client);

		client = tmp;
	}
}

/** \fn void worker_termination_handler(int signum)
 *  \brief Обработчик сигнала для завершения работы дочернего процесса по SIGHUP или SIGINT
 *  \return Ничего.
 */
void worker_termination_handler(int signum)
{
	if (signum == SIGHUP || signum == SIGINT) {
		info("exiting: parent process finished");
		close_clients();
		info("worker finished");
		exit(0);
	}
}

/** \fn void termination_handler(int signum)
 *  \brief Обработчик сигнала для завершения работы главного процесса по SIGINT
 *  \return Ничего.
 */
void termination_handler(int signum)
{
	if (signum == SIGINT) {
		info("exiting main process: sigint");
		close_clients();
		if (close(server_socket) < 0) {
			error("error closing server socket: %s", strerror(errno));
		}
		info("main process finished");
		exit(0);
	}
}

/** \fn void run()
 *  \brief Запуск рабочих процессов
 *  \return Ничего.
 *  Запускает посредством вызвов fork() рабочие процессы и устанавливает обработчики сигналов.
 *  Настраивает доставку сигнала SIGHUP дочерним процессам при завершении родительского процесса.
 */
int run()
{
	info("serving at %s:%d with pool of %d processes",
	     global_settings.address, global_settings.port, global_settings.processes_count);

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		error("can't open socket");
		return sock;
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons (global_settings.port);
	inet_pton(AF_INET, global_settings.address, &(addr.sin_addr));

	int so_reuseaddr = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &so_reuseaddr, sizeof(so_reuseaddr));
	fcntl(sock, F_SETFL, O_NONBLOCK);

	int r;
	if ((r = bind(sock, (struct sockaddr *) &addr, sizeof(addr))) < 0) {
		error("can't bind socket");
		return r;
	}

	my_worker_id = 0;
	listen(sock, SOMAXCONN);
	server_socket = sock;
	signal(SIGINT, termination_handler);

	for (int i = 1; i < global_settings.processes_count; i++) {
		int pid = fork();
		if (pid == 0) {
			my_worker_id = i;
			prctl(PR_SET_PDEATHSIG, SIGHUP);
			signal(SIGINT, worker_termination_handler);
			signal(SIGHUP, worker_termination_handler);
			break;
		}
	}


	return serve();
}
