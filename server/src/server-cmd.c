#include "server-cmd.h"

/** \file server-cmd.c
 *  \brief Функции переходов.
 *
 *   Функции обработки передоходов конечного автомата.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <limits.h>
#include <errno.h>

#include "logger.h"
#include "hostutil.h"
#include "settings.h"
#include "server-parse.h"

extern ServerSettings global_settings;

/** \fn int make_error(const char*, Client*);
 *  \brief Функция, обрабатывающая ошибку при обработке других команд.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_error(const char* cmd, Client* client) {
	if (client->reply == NULL) {
		warn("%d unexpected command: %s", client->id, cmd);
		fill_reply(client, "503 Unexpected command\r\n");
	}
	return 0;
}

/** \fn int make_helo(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду HELO.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_helo(const char* cmd, Client* client) {
	client_clear_buffers(client);

	char buf[1024];
	sprintf(buf, "250 %s\r\n", global_settings.domain);
	fill_reply(client, buf);
	return 0;
}

/** \fn int make_ehlo(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду EHLO.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_ehlo(const char* cmd, Client* client) {
	client_clear_buffers(client);

	const char* greetings = "sup /b/";
	char buf[1024];
	sprintf(buf, "250 %s %s\r\n", global_settings.domain, greetings);
	fill_reply(client, buf);
	return 0;
}

/** \fn int make_rset(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду RSET.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_rset(const char* cmd, Client* client) {
	client_clear_buffers(client);
	fill_reply(client, "250 OK\r\n");
	return 0;
}

/** \fn int make_vrfy(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду VRFY.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_vrfy(const char* cmd, Client* client) {
	fill_reply(client, "252 Know nothing about users\r\n");
	return 0;
}

int make_quit(const char* cmd, Client* client) {
	client_clear_buffers(client);

	char answer[1024];
	sprintf(answer, "221 %s closing transmission channel, bye-bye!\r\n", global_settings.domain);
	fill_reply(client, answer);
	client->finished = 1;
	return 0;
}

/** \fn int make_timeout(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая событие таймаута.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_timeout(const char* cmd, Client* client) {
	client_clear_buffers(client);

	char answer[1024];
	sprintf(answer, "421 %s Timeout exceeded\r\n", global_settings.domain);
	fill_reply(client, answer);
	client->finished = 1;
	return 0;
}

/** \fn int make_mail(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду MAIL.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_mail(const char* cmd, Client* client) {
	client_clear_buffers(client);

	char* answer;

	char* from_str = get_param(SERVER_FSM_EV_MAIL, cmd);
	if (from_str == NULL) {
		answer = "501 Wrong from\r\n";
	} else {
		answer = "250 OK\r\n";
		client->mail->from = from_str;
		info("%d mail from: '%s'", client->id, client->mail->from);
	}

	fill_reply(client, answer);

	return 0;
}

/** \fn int make_rcpt(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду RCPT.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_rcpt(const char* cmd, Client* client) {
	char* answer;

	char* next_recipient = get_param(SERVER_FSM_EV_RCPT, cmd);
	if (next_recipient == NULL) {
		answer = "501 Wrong recipient\r\n";
	} else {
		char* domain = get_email_domain(next_recipient);
		debug("%d checking recipient: %s -> domain: %s", client->id, next_recipient, domain);
		if (domain == NULL) {
			answer = "501 Wrong recipient\r\n";
			free(next_recipient);
		} else {
			int domain_available = 0;
			int current_is_mine = 0;
			if (strcmp(domain, global_settings.domain) == 0) {
				// my domain
				client->my_mail = 1;
				domain_available = 1;
				current_is_mine = 1;
			} else {
				//check if can relay
				for (int i = 0; i < global_settings.relay_domains_count; i++) {
					if (strcmp(domain, global_settings.relay_domains[i]) == 0) {
						domain_available = 1;
						break;
					}
				}
			}
			free(domain);

			if (domain_available) {
				answer = "250 OK\r\n";
				if (!current_is_mine) {
					StringEl* recipient = malloc(sizeof(StringEl));
					recipient->content = next_recipient;
					LIST_INSERT_HEAD(&client->mail->recipients, recipient, next);
				} else {
					free(next_recipient);
				}
			} else {
				answer = "550 Can't relay to that domain\r\n";
				free(next_recipient);
			}
		}
	}

	fill_reply(client, answer);
	return 0;
}

/** \fn int make_data(const char* cmd, Client* client);
 *  \brief Функция, обрабатывающая команду DATA.
 *  \param cmd -- текст команды;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int make_data(const char* cmd, Client* client) {
	if (LIST_FIRST(&client->mail->recipients) == NULL && !client->my_mail) {
		debug("%d data rejected: recipients list empty", client->id);
		fill_reply(client, "554 No valid recipients\r\n");
		return 1;
	}
	client->content_finished = 0;
	fill_reply(client, "354 Enter message, ending with \\r\\n.\\r\\n\r\n");
	return 0;
}

/** \fn int add_content(const char* cmd, Client* client)
 *  \brief Заполнение буфера письма
 *  \return 0.
 *  Заполняет буфер получаемого письма.
 *  Если размер письма превышает максимальный заданный в настройках - сообщает об ошибке
 *  и сбрасывает буфер.
 *  Определяет, что письмо завершилось.
 */
int add_content(const char* cmd, Client* client) {
	int len = strlen(cmd);
	int new_size;
	if (client->mail->data == NULL) {
		new_size = len;
		client->mail->data = malloc(sizeof(char) * (len + 1));
		strcpy(client->mail->data, cmd);
	} else {
		int current_size = strlen(client->mail->data);
		if (current_size + len > global_settings.max_data_length) {
			client_clear_buffers(client);
			error("%d received message is too large (%d bytes), drop buffers", client->id, current_size + len);
			fill_reply(client, "552 Message too large\r\n");
			client->content_finished = 2;
			return 0;
		}

		new_size = current_size + len;
		client->mail->data = realloc(client->mail->data, sizeof(char) * (new_size + 1));
		strcpy(client->mail->data + current_size, cmd);
	}

	if (new_size >= 5) {
		if (strcmp(&client->mail->data[new_size - 5], "\r\n.\r\n") == 0) {
			client->content_finished = 1;
			client->mail->data[new_size - 3] = '\0'; // cut last \r\n
		}
	}
	return 0;
}

/** \fn int store_data(const char* maildir, const char* fname, const char* subfloder, const char* data, int size)
 *  \brief Безопасное сохранение файлан в папку
 *  \param maildir - корневой каталог, в котором есть папка tmp
 *  \param fname - имя файла
 *  \param subfolder - в какую папку корневого каталога надо сохранить
 *  \param data - данные
 *  \param size - размер данных
 *  \return 0 или ошибку при работе с файловой системой.
 *  Записываем переданные данные в папку tmp, а
 *  затем, в случае удачной записи, перемещает файл из tmp
 *  в заданную папку.
 */
int store_data(const char* maildir, const char* fname,
               const char* subfloder, const char* data, int size) {
	char tmp_name[PATH_MAX];
	sprintf(tmp_name, "%s/%s/%s", maildir, "tmp", fname);
	debug("storing data to %s", tmp_name);

	FILE *fp;
	if ( !(fp=fopen(tmp_name, "w")) ) {
		error("Can't open file '%s' to write: %s", tmp_name, strerror(errno));
		return -errno;
	}

	if ( fwrite(data, sizeof(char), size, fp) <= 0 ) {
		error("Can't write to '%s': %s", tmp_name, strerror(errno));
		free(tmp_name);
		return -errno;
	}
	fclose(fp);

	char new_name[PATH_MAX];
	sprintf(new_name, "%s/%s/%s", maildir, subfloder, fname);
	debug("moving %s to %s", tmp_name, new_name);
	if ( rename(tmp_name, new_name) == -1 ) {
		error("Can't move '%s' to '%s': %s", tmp_name, new_name, strerror(errno));
		return -errno;
	}
	return 0;
}

/** \fn void append_line_to_mail_data(Client* client, char* header)
 *  \brief Добавляет указанный заголовок к письму
 */
void append_line_to_mail_data(Client* client, char* header) {
	int mail_data_len = strlen(client->mail->data);
	int header_len = strlen(header);
	char* middle_data = malloc(sizeof(char) * (mail_data_len + header_len + 1));
	strcpy(middle_data, header);
	strcpy((middle_data + header_len), client->mail->data);
	free(client->mail->data);
	client->mail->data = middle_data;
}

/** \fn int transit_mail(Client* client, const char* filename)
 *  \brief Поместить письмо в папку очереди удалённой отправки
 */
int transit_mail(Client* client, const char* filename) {
	char* data = NULL;
	int data_size = marshall_mail_to_string(client->mail, &data);
	if (data_size == 0) {
		error("mail marshaling failed");
		return 1;
	} else {
		if (store_data(global_settings.maildir, filename, "transit", data, data_size)) {
			if (data != NULL) free(data);
			return 1;
		}
	}
	if (data != NULL) free(data);
	return 0;
}

/** \fn int store_mail_localy(Client* client, const char* filename)
 *  \brief Поместить письмо в папку new в локальный почтовый ящик
 */
int store_mail_localy(Client* client, const char* filename) {
	if (!client->my_mail) return 0;

	char return_path[1024];
	sprintf(return_path, "Return-Path: %s\r\n", client->mail->from);
	append_line_to_mail_data(client, return_path);

	return store_data(global_settings.maildir, filename, "new",
	                  client->mail->data, strlen(client->mail->data));
}

/** \fn store_data_on_disk(Client* client);
 *  \brief Функция, отвечающая за сохранение письма в почтовом ящике на диске.
 *  \param cmd -- часть письма;
 *  \param state -- состояние сервера.
 *  \return 0 -- в случае успеха, или код ошибки.
 */
int store_data_on_disk(Client* client) {
	if (client->content_finished != 1) {
		// message was broken, just go through
		client_clear_buffers(client);
		return 0;
	}

	debug("%d store_data_on_disk: from='%s' data='%s'", client->id, client->mail->from, client->mail->data);

	// insert timestamp line
	char timestamp[1024];
	char* from_domain = get_email_domain(client->mail->from);
	char time_str[30];
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(time_str, 30, "%d %b %Y %H:%M:%S %z", timeinfo);
	sprintf(timestamp, "Received: from %s ([%s]) by %s ; %s\r\n",
	        (from_domain == NULL ? "" : from_domain),
	        client->connection.client_ip, global_settings.domain, time_str);
	if (from_domain != NULL) free(from_domain);
	append_line_to_mail_data(client, timestamp);

	char filename[1024];

	sprintf(filename, "%d.%ld.bson", client->id, time(0));
	if (LIST_FIRST(&client->mail->recipients) != NULL) {
		if (transit_mail(client, filename)) {
			goto _error;
		}
	}

	sprintf(filename, "%d.%ld.eml", client->id, time(0));
	if (store_mail_localy(client, filename)) {
		goto _error;
	}

	fill_reply(client, "250 OK\r\n");
	client_clear_buffers(client);
	return 0;

_error:
	client_clear_buffers(client);
	fill_reply(client, "452 Insufficient system storage\r\n");
	return 0;
}
