#include "perform.h"

/** \file perform.c
 *  \brief Обработка событий.
 *
 *   Определение произошедшего события и прогон конечного автомата.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>

#include "logger.h"
#include "hostutil.h"
#include "settings.h"
#include "connection.h"
#include "server-parse.h"
#include "server-fsm.h"
#include "server-cmd.h"

extern ServerSettings global_settings;

const char* event_to_string(int event) {
	switch (event) {
	case SERVER_FSM_EV_HELO:
		return "helo";
	case SERVER_FSM_EV_EHLO:
		return "ehlo";
	case SERVER_FSM_EV_QUIT:
		return "quit";
	case SERVER_FSM_EV_RSET:
		return "rset";
	case SERVER_FSM_EV_MAIL:
		return "mail";
	case SERVER_FSM_EV_RCPT:
		return "rcpt";
	case SERVER_FSM_EV_VRFY:
		return "vrfy";
	case SERVER_FSM_EV_DATA:
		return "data";
	case SERVER_FSM_EV_DATA_TERMINATE:
		return "data_terminate";
	case SERVER_FSM_EV_ERROR:
		return "error";
	default:
		return "unknown";
	}
}

const char* state_to_string(int state) {
	switch (state) {
	case SERVER_FSM_ST_INVALID:
		return "invalid";
	case SERVER_FSM_ST_INIT:
		return "init";
	case SERVER_FSM_ST_DONE:
		return "done";
	case SERVER_FSM_ST_OPENED:
		return "opened";
	case SERVER_FSM_ST_FROM:
		return "from";
	case SERVER_FSM_ST_RECIPIENT:
		return "recipient";
	case SERVER_FSM_ST_CONTENT:
		return "content";
	default:
		return "unknown";
	}
}

void send_bad_command(Client* client) {
	fill_reply(client, "500 Bad command\r\n");
}

/** \fn void initialize()
 *  \brief Ответить приветствием в только что открывшееся соединение
 *  \return Ничего.
 */
void initialize(Client* client) {
	char answer[1024];
	sprintf(answer, "220 %s at your service\r\n", global_settings.domain);
	fill_reply(client, answer);
}

/** \fn void check_timeout()
 *  \brief Проверка, не истёк ли таймаут для данного клиента
 *  \return Ничего.
 *  Если истёк - инициирует переход по событию timeout.
 */
void check_timeout(Client* client) {
	time_t now = time(0);
	int expire_timeout = global_settings.timeout; // s.
	if ((now - client->last_activity) > expire_timeout) {
		te_server_fsm_state next = server_fsm_step(client->state, SERVER_FSM_EV_TIMEOUT, "", client);
		info("%d timeout: state='%s' event='%s' next-state='%s'", client->id,
		     state_to_string(client->state), event_to_string(SERVER_FSM_EV_TIMEOUT),
		     state_to_string(next));

		assert(next != SERVER_FSM_ST_INVALID);
		client->state = next;
	}
}

/** \fn void perform()
 *  \brief Обработка поступивших от клиента данных
 *  \return Ничего.
 *  Определяет случившееся событие и вызывает переход в новое состояние.
 *  При необходимости делает дополнительный переход по событию error.
 */
int perform(Client* client, char command[MAXCMDLEN], int size) {
	te_server_fsm_event event;
	client->last_activity = time(0);

	if (client->state == SERVER_FSM_ST_CONTENT) {
		event = SERVER_FSM_EV_DATA;
	} else {
		// parse command
		if (command[size - 1] != '\n') {
			warn("%d no \\n in command: %s", client->id, command);
			send_bad_command(client);
			return CONTINUE;
		}

		event = parse_cmd(command);
		if (event == SERVER_FSM_EV_INVALID) {
			warn("%d command unrecognized: %s", client->id, command);
			send_bad_command(client);
			return CONTINUE;
		}

		char tmp = command[size - 1]; // hack for pretty print command
		command[size - 1] = '\0';
		info("%d performing: command='%s'", client->id, command);
		command[size - 1] = tmp;
	}

	te_server_fsm_state next = server_fsm_step(client->state, event, command, client);
	info("%d performing: state='%s' event='%s' next-state='%s'", client->id,
	     state_to_string(client->state), event_to_string(event), state_to_string(next));

	// additional hop if data finished
	if (next != SERVER_FSM_ST_INVALID && client->content_finished) {
		event = SERVER_FSM_EV_DATA_TERMINATE;
		next = server_fsm_step(client->state, event, "", client);
		info("%d performing: state='%s' event='%s' next-state='%s'", client->id,
		     state_to_string(client->state), event_to_string(event), state_to_string(next));
	}

	if (next == SERVER_FSM_ST_INVALID) {
		event = SERVER_FSM_EV_ERROR;
		next = server_fsm_step(client->state, event, "", client);
		info("%d performing: state='%s' event='%s' next-state='%s'", client->id,
		     state_to_string(client->state), event_to_string(event), state_to_string(next));
	}

	client->state = next;
	info("%d in state '%s'", client->id, state_to_string(client->state));
	return client->state == SERVER_FSM_ST_DONE ? DONE : CONTINUE;
}