/** \file server-parse.c
 *  \brief Функции и переменные для разбора команд.
 *
 *  В этом файле описаны функции по разбору команд и их параметров
 *  с помощью регулярных выражений.
 *  В файле существует функция инициализации, компилирующая регулярные выражения.
 */

#include <pcre.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "logger.h"
#include "server-parse.h"
#include "server-re.h"

enum command_re_types {
	command_helo = 0,
	command_ehlo,
	command_quit,
	command_rset,
	command_mail,
	command_rcpt,
	command_vrfy,
	command_data,
	command_re_count
};

enum params_re_types {
	mail_from_params = 0,
	rcpt_to_params,
	params_re_count
};

static const char* commands_expressions[command_re_count] = {
	RE_CMD_HELO,
	RE_CMD_EHLO,
	RE_CMD_QUIT,
	RE_CMD_RSET,
	RE_CMD_MAIL,
	RE_CMD_RCPT,
	RE_CMD_VRFY,
	RE_CMD_DATA
};

static const char* params_expressions[params_re_count] = {
	RE_CMD_MAIL_PARAM,
	RE_CMD_RCPT_PARAM
};

static const int events[command_re_count] = {
	SERVER_FSM_EV_HELO,
	SERVER_FSM_EV_EHLO,
	SERVER_FSM_EV_QUIT,
	SERVER_FSM_EV_RSET,
	SERVER_FSM_EV_MAIL,
	SERVER_FSM_EV_RCPT,
	SERVER_FSM_EV_VRFY,
	SERVER_FSM_EV_DATA
};

static const int events_with_params[params_re_count] = {
	SERVER_FSM_EV_MAIL,
	SERVER_FSM_EV_RCPT
};

typedef struct {
	te_server_fsm_event event;
	pcre* re;
} ReStruct;

static ReStruct re_events[command_re_count];
static pcre* re_params[params_re_count];

te_server_fsm_event parse_cmd(const char *cmd)
{
	int i;
	int len = strlen(cmd);
	int ovector[6];

	for (i = 0; i < command_re_count;  i++) {
		int match = pcre_exec(re_events[i].re, NULL, cmd, len, 0, PCRE_ANCHORED, ovector, 6);
		if (match > 0) {
			return re_events[i].event;
		}
	}
	return SERVER_FSM_EV_INVALID;
}

char *get_param(te_server_fsm_event event, const char *cmd)
{
	int i;
	int len = strlen(cmd);
	int ovector[9];

	for (i = 0; i < params_re_count;  i++) {
		if (events_with_params[i] == event) {
			int match = pcre_exec(re_params[i], NULL, cmd, len, 0, PCRE_ANCHORED, ovector, 6);
			if (match > 0) {
				const char *sp = NULL;
				pcre_get_substring((const char *)cmd, ovector, match, 1, &sp);

				char* result = malloc((strlen(sp) + 1) * sizeof(char));

				strcpy(result, sp);
				pcre_free_substring(sp);

				return result;
			}
		}
	}
	return NULL;
}


#define COMPILE_REG(regex, expression) { 																	\
	const char *errstr;																	\
	int errchar;																		\
	if ( !(regex = pcre_compile(expression, PCRE_UTF8 | PCRE_CASELESS, &errstr, &errchar, NULL)) ) {	\
		error("Can't compile regexp: %s on %d", errstr, errchar);					\
		assert(0);																	\
	}																					\
}

void init_re()
{
	for (int i = 0; i < command_re_count; i++) {
		re_events[i].event = events[i];
		COMPILE_REG(re_events[i].re, commands_expressions[i]);
	}
	for (int i = 0; i < params_re_count; i++) {
		COMPILE_REG(re_params[i], params_expressions[i]);
	}
	info("regexps inited");
}

/* Косяк */
void __attribute__((constructor)) initre()
{
	init_re();
}

void __attribute__((destructor)) freere()
{
	//free_re();
}
