#pragma once

/** \file client.h
 *  \brief Cтруктура Client.
 *
 *   Описание структуры для хранения информации о текущей обработке клиентского соединения.
 */

#include <sys/queue.h>
#include <string.h>
#include <time.h>

#include "mail.h"
#include "connection.h"
#include "server-fsm.h"

/**
 * \brief Состоение обработки клиентского соединения
 */
typedef struct Client {
	Connection connection;	///< Информация о tcp-соединении
	int id;	///< Уникальный идентификатор данного клиентского соединения
	te_server_fsm_state state;	///< Состояние конечного автомата сервера
	Mail* mail; ///< Информация о письме
	char* reply; ///< Последний ответ сервера, который ожидает отправки
	unsigned finished; 	///< Завершена ли обработка данного клиента (чтобы не делать лишний раз poll на нём)
	unsigned content_finished;	///< Завершен ли ввод письма
	unsigned my_mail;	///< Предназначено ли письмо домену сервера
	time_t last_activity;	///< Время последней активности клиента

	SLIST_ENTRY(Client) next;
} Client;

typedef SLIST_HEAD(,Client) ClientsList;

Client* new_client(int id, int client_sock, int port, ip_str_t client_ip);

void client_clear_buffers(Client* client);

void delete_client(Client** client);

void fill_reply(Client* client, char* const_str);
