#pragma once

#include "client.h"
#include "server-cmd.h"

#define INVALID_COMMAND -1
#define DONE 1
#define CONTINUE 0

void initialize(Client* client);

void check_timeout(Client* client);

// parse input and perform command with smtp-state-machine
int perform(Client* client, char command[MAXCMDLEN], int size);
