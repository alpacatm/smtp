#pragma once

/** \file settings.h
 *  \brief Настройки.
 *
 *   Объявление структуры настроек системы, содержащей сетевой адрес привязки сервера,
 *   количество рабочих процессов, путь к почтовому ящику, время старта системы,
 *   используемое доменное имя, известные домены для релея и другие штуки.
 */

#include <time.h>

#include "connection.h"

/**
 * \brief Настройки системы
 */
typedef struct {
	const char* address;
	int port;
	int processes_count;
	time_t start_time;
	const char* maildir;
	const char* domain;
	const char** relay_domains;
	int relay_domains_count;
	int timeout;
	int max_data_length;
} ServerSettings;

ServerSettings global_settings;