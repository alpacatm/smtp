#pragma once

/** \file client.h
 *  \brief Cтруктура Connection.
 *
 *   Описание структуры Connection, используемой для хранения информации о
 *   входящем соединении.
 */

#include <netdb.h>

typedef char ip_str_t[INET_ADDRSTRLEN];

typedef struct Connection {
	int sock; ///< Дескриптор сокета
	int port; ///< Порт клиента
	ip_str_t client_ip; ///< Клиентский ip-адрес в виде строки
} Connection;
