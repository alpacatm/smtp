#pragma once

/** \file server-re.h
 *  В этом файле описаны макросы регулярных выражений.
 */

#define RE_CMD_QUIT "^\\s*QUIT\\s*\n$"
#define RE_CMD_HELO "^\\s*HELO\\s"
#define RE_CMD_EHLO "^\\s*EHLO\\s"
#define RE_CMD_RSET "^\\s*RSET\\s*\n$"
#define RE_CMD_VRFY "^\\s*VRFY .+\\s*\n$"
#define RE_CMD_DATA "^\\s*DATA\\s*\n$"

#define ADL "\\@(?:(?:[a-zA-Z]+(?:[a-zA-Z\\d\\-]+[a-zA-Z\\d])?|#\\d+|(?:(?:[0-1]?\\d?\\d|2[0-5]\\d|25[0-5])(?:\\.[0-1]?\\d?\\d|2[0-5]\\d|25[0-5]){3}))(?:\\.(?:[a-zA-Z]+(?:[a-zA-Z\\d\\-]+[a-zA-Z\\d])?|#\\d+|(?:(?:[0-1]?\\d?\\d|2[0-5]\\d|25[0-5])(?:\\.[0-1]?\\d?\\d|2[0-5]\\d|25[0-5]){3})))*)(?:\\s*,\\s*\\@(?:(?:[a-zA-Z]+(?:[a-zA-Z\\d\\-]+[a-zA-Z\\d])?|#\\d+|(?:(?:[0-1]?\\d?\\d|2[0-5]\\d|25[0-5])(?:\\.[0-1]?\\d?\\d|2[0-5]\\d|25[0-5]){3}))(?:\\.(?:[a-zA-Z]+(?:[a-zA-Z\\d\\-]+[a-zA-Z\\d])?|#\\d+|(?:(?:[0-1]?\\d?\\d|2[0-5]\\d|25[0-5])(?:\\.[0-1]?\\d?\\d|2[0-5]\\d|25[0-5]){3})))*))*\\s*:"
#define MAILBOX "(?:(?:[^\\s<>\\(\\)\\[\\]\\\\\\.,;:\\@\"0x0-0x1f0x7f]|\\\\.)+(?:\\.(?:[^\\s<>\\(\\)\\[\\]\\\\\\.,;:\\@\"0x0-0x1f0x7f]|\\\\.)+)*|\"(?:\\\\.+|[^\\r\\n\"\\\\]+)\")\\@(?:(?:[a-zA-Z]+(?:[a-zA-Z\\d\\-]+[a-zA-Z\\d])?|#\\d+|(?:(?:[0-1]?\\d?\\d|2[0-5]\\d|25[0-5])(?:\\.[0-1]?\\d?\\d|2[0-5]\\d|25[0-5]){3}))(?:\\.(?:[a-zA-Z]+(?:[a-zA-Z\\d\\-]+[a-zA-Z\\d])?|#\\d+|(?:(?:[0-1]?\\d?\\d|2[0-5]\\d|25[0-5])(?:\\.[0-1]?\\d?\\d|2[0-5]\\d|25[0-5]){3})))*)"

#define REVERSE_PATH "<\\s*(?:"ADL")?\\s*("MAILBOX"|)\\s*>"
#define FORWARD_PATH "<\\s*(?:"ADL")?\\s*("MAILBOX")\\s*>"

#define RE_CMD_MAIL "^\\s*MAIL\\s+FROM\\s*:"
#define RE_CMD_MAIL_PARAM "^\\s*MAIL\\s+FROM\\s*:\\s*"REVERSE_PATH"\\s*\n$"

#define RE_CMD_RCPT "^\\s*RCPT\\s+TO\\s*:"
#define RE_CMD_RCPT_PARAM "^\\s*RCPT\\s+TO\\s*:\\s*"FORWARD_PATH"\\s*\n$"
