#pragma once

/** \file server-cmd.h
 *  \brief Функции обработки команд и изменения состояния сервера.
 *
 *   В этом файле описано состояние сервера, а также вызываемые
 *   из конечного автомата функции обработки команд.
 */

#include "client.h"

#define MAXCMDLEN 1024

int make_error(const char* cmd, Client* client);
int make_helo(const char* cmd, Client* client);
int make_ehlo(const char* cmd, Client* client);
int make_quit(const char* cmd, Client* client);
int make_timeout(const char* cmd, Client* client);
int make_rset(const char* cmd, Client* client);
int make_vrfy(const char* cmd, Client* client);
int make_mail(const char* cmd, Client* client);
int make_rcpt(const char* cmd, Client* client);
int make_data(const char* cmd, Client* client);
int add_content(const char* cmd, Client* client);
int store_data_on_disk(Client* client);