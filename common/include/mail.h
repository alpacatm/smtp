#pragma once

#include <sys/queue.h>

typedef struct StringEl {
    char* content;
    LIST_ENTRY(StringEl) next;
} StringEl;

typedef LIST_HEAD(,StringEl) StringList;

typedef struct Mail {
    char* from;
    char* data;
    char* x_original_to;
    StringList recipients;
} Mail;

Mail* new_mail();
void clear_mail(Mail* mail);
void free_mail(Mail** mail);
int marshall_mail(const Mail* mail, const char* file);
int marshall_mail_to_string(const Mail* mail, char** custom_buffer);
Mail* demarshall_mail(const char* file);