#pragma once

int init_logger(const char* logbase);

int info(const char *fmt, ...);

int debug(const char *fmt, ...);

int error(const char *fmt, ...);

int warn(const char *fmt, ...) ;
