#include "hostutil.h"
#include "logger.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <resolv.h>
#include <unistd.h>
#include <pcre.h>

int connect_to_mx(const char *host, uint32_t port, uint32_t retries, uint32_t interval, int timeout) {
#define buf_size 100
    int sfd = 0;
    for ( ; retries > 0 && !sfd; retries-- ) {
        struct addrinfo hints, *res;
        int errcode;
        char addrstr[buf_size];
        void *ptr;

        memset(&hints, 0, sizeof (hints));
        hints.ai_family = PF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags |= AI_CANONNAME;

        errcode = getaddrinfo(host, NULL, &hints, &res);
        if ( errcode != 0 ) {
            error("Can't getaddrinfo('%s')", host, strerror(errno));
            return -1;
        }

        while ( res ) {
            inet_ntop(res->ai_family, res->ai_addr->sa_data, addrstr, buf_size);

            switch ( res->ai_family ) {
            case AF_INET:
                ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
                break;
            case AF_INET6:
                ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
                break;
            }
            inet_ntop(res->ai_family, ptr, addrstr, buf_size);

            sfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

            struct timeval tdout;
            tdout.tv_sec = timeout;
            tdout.tv_usec = 0;
            setsockopt(sfd, SOL_SOCKET, SO_SNDTIMEO, &tdout, 0x10);
            setsockopt(sfd, SOL_SOCKET, SO_RCVTIMEO, &tdout, 0x10);

            ((struct sockaddr_in*)res->ai_addr)->sin_port = htons(port);
            if( connect(sfd, res->ai_addr, res->ai_addrlen) >= 0 ) {
                info("Connected to %s via IPv%d address: %s (%s)",
                     host,
                     res->ai_family == PF_INET6 ? 6 : 4,
                     addrstr,
                     res->ai_canonname
                    );
                break;
            }
            info("Can't connect to '%s': %s",
                 host,
                 ((errno == EINPROGRESS || errno == ETIMEDOUT) ? "timeout" : strerror(errno))
                );
            close(sfd);
            sfd = 0;

            res = res->ai_next;
        }
        if ( !sfd ) sleep(interval);
    }

    return sfd;
}

char* get_domain_mx(const char* domain) {
    u_char nsbuf[4096];

    ns_msg handle;
    ns_rr record;

    uint32_t out_size = res_query(domain, ns_c_any, ns_t_mx, nsbuf, sizeof (nsbuf));
    if (out_size <= 0 || ns_initparse(nsbuf, out_size, &handle) == -1) {
        warn("Can't resolve '%s' mx, use it as mx host", domain);
        char* res = malloc((strlen(domain) + 1) * sizeof(char));
        strcpy(res, domain);
        return res;
    }
    uint32_t records = ns_msg_count(handle, ns_s_an);

    u_int best_mx = 0;
    char* mx = NULL;

    for (int i = 0; i < records; i++) {
        if (ns_parserr(&handle, ns_s_an, i, &record) < 0) {
            error("Can't parse resolve result for %s: %s", domain, strerror(errno));
            continue;
        }
        char cname[1024];
        u_char const *p = (u_char*) ns_rr_rdata(record);
        int res = ns_name_uncompress(ns_msg_base(handle), ns_msg_end(handle), p + 2, cname, 1024);
        if (res < 0) continue;
        if (mx == NULL || best_mx > ns_get16(p)) {
            best_mx = ns_get16(p);
            if (mx) free(mx);
            mx = malloc((strlen(cname) + 1) * sizeof (char));
            strcpy(mx, cname);
        }
    }

    if ( mx == NULL ) error("There is no good mx records for '%s'", domain);
    return mx;
}


pcre* _MAIL_RX = NULL;
char* get_email_domain(const char* email) {
    if ( !_MAIL_RX ) {
        const char *errstr;
        int errchar;
        if ( !(_MAIL_RX = pcre_compile(MAIL_RE, PCRE_UTF8 | PCRE_CASELESS, &errstr, &errchar, NULL)) ) {
            error("Can't compile mail regexp: %s on %d", errstr, errchar);
            return NULL;
        }
    }

    int ovector[6];
    int match = pcre_exec(_MAIL_RX, NULL, email, strlen(email), 0, PCRE_ANCHORED, ovector, 6);
    if ( match != 2 ) {
        return NULL;
    }

    const char *sp = NULL;
    pcre_get_substring((const char *)email, ovector, match, 1, &sp);

    char* domain = malloc((strlen(sp) + 1) * sizeof(char));

    strcpy(domain, sp);
    pcre_free_substring(sp);

    return domain;
}

char* get_hostname() {
    char* buf = malloc((HOST_NAME_MAX + 1) * sizeof(char));
    gethostname(buf, HOST_NAME_MAX);
    return buf;
}