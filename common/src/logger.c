#include "logger.h"

#include "stdio.h"
#include "stdarg.h"
#include "unistd.h"
#include "errno.h"
#include "stdlib.h"
#include "string.h"
#include  <signal.h>

#include "pthread.h"

FILE* log_pipe = 0;
pthread_mutex_t log_lock;

typedef enum {
    DEBUG,
    INFO,
    WARN,
    ERROR
} log_type;

int init_logger(const char* logbase) {
    if (!log_pipe) {
        pthread_mutex_init(&log_lock, NULL);

        int pipes[2];
        if (pipe(pipes)) {
            perror("pipe failed");
            return -1;
        }


        char* log_name = malloc((strlen(logbase) + 5) * sizeof(char));
        sprintf(log_name, "%s.log", logbase);
        FILE* out = fopen(log_name, "a");
        if ( !out ) {
            fprintf(stderr, "Can't open log file '%s': %s\n", log_name, strerror(errno));
            free(log_name);
            return -1;
        }

        sprintf(log_name, "%s.err", logbase);
        FILE* err = fopen(log_name, "a");
        if ( !err ) {
            fprintf(stderr, "Can't open log file '%s': %s\n", log_name, strerror(errno));
            free(log_name);
            return -1;
        }

        free(log_name);

        int child = fork();
        if (child == -1) {
            perror("fork failed");
            return -1;
        }

        if ( child == 0 ) {
            close(pipes[1]);

            FILE* stream = fdopen(pipes[0], "r");
            if ( !stream ) {
                perror("Can't open parents pipe");
                _exit(EXIT_FAILURE);
            }

            int type;
            unsigned long length;
            signal(SIGINT, SIG_IGN);
            signal(SIGPIPE, SIG_IGN);
            while ( fscanf(stream, "%d %lu ", &type, &length) == 2 ) {
                char* msg = malloc((length + 1) * sizeof (char));
                fread(msg, sizeof(char), length, stream);
                msg[length] = '\0';
                fprintf(out, "%s", msg);
                fflush(out);
                if ( type >= WARN ) {
                    fprintf(err, "%s", msg);
                    fflush(err);
                }
                free(msg);
            }
            fclose(stream);
            fclose(err);
            fclose(out);
            _exit(EXIT_SUCCESS);
        } else {
            fclose(out);
            fclose(err);
            close(pipes[0]);
            if ( !(log_pipe = fdopen(pipes[1], "w")) ) {
                perror("Can't open log pipe");
                return -1;
            }
        }
    } else {
        warn("logger already inited");
    }

    return 0;
}

char* _prepare_log_message(const char* type, const char* fmt, va_list ap) {
    time_t rawtime;
    struct tm * timeinfo;
    char buffer [20];

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 20, "%Y/%m/%d %H:%M:%S", timeinfo);

    int title_size = snprintf(NULL, 0, "%s %d %s\t ", buffer, getpid(), type);

    va_list copy;
    va_copy(copy, ap);
    int content_size = vsnprintf(NULL, 0, fmt, copy);
    va_end(copy);

    char* result = malloc(sizeof (char*) * (title_size + content_size + 2));  // \n and \0
    sprintf(result, "%s %d %s\t ", buffer, getpid(), type);
    vsnprintf(result + title_size, content_size + 1, fmt, ap);
    sprintf(result + title_size + content_size, "\n");
    return result;
}

int _send_log_message(log_type type, char* msg) {
    pthread_mutex_lock(&log_lock);
    int result = log_pipe ?
                 fprintf(log_pipe, "%d %lu %s", type, strlen(msg), msg) :
                 fprintf(stderr, "%s", msg);
    if ( result > 0 && log_pipe ) fflush(log_pipe);

    if ( result <= 0 ) perror("fprintf");
    pthread_mutex_unlock(&log_lock);
    return result < 0 ? result : 0;
}

#define LOG(severity) {                                             \
    va_list ap;                                                     \
    va_start(ap, fmt);                                              \
    char *msg = _prepare_log_message(#severity, fmt, ap);           \
    int result = _send_log_message(severity, msg);                  \
    free(msg);                                                      \
    return result;                                                  \
}

int info(const char *fmt, ...) LOG(INFO);

int debug(const char *fmt, ...) LOG(DEBUG);

int error(const char *fmt, ...) LOG(ERROR);

int warn(const char *fmt, ...) LOG(WARN);
