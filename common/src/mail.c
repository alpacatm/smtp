#include "mail.h"

#include "logger.h"

#include <stdlib.h>
#include <bson.h>
#include <errno.h>

#define _sfree(x) if ( x != NULL ) free(x);

#define FREE_AND_NULL(a) \
    if (a != NULL) {     \
        free(a);         \
        a = NULL;        \
    }

Mail* new_mail() {
    Mail* result = malloc(sizeof(Mail));

    result->from = NULL;
    result->data = NULL;
    result->x_original_to = NULL;

    LIST_INIT(&result->recipients);

    return result;
}

void clear_mail(Mail* mail) {
    FREE_AND_NULL(mail->from);
    FREE_AND_NULL(mail->data);
    FREE_AND_NULL(mail->x_original_to);

    StringEl *n2, *n1 = LIST_FIRST(&mail->recipients);
    while ( n1 != NULL ) {
        n2 = LIST_NEXT(n1, next);
        LIST_REMOVE(n1, next);
        _sfree(n1->content);
        free(n1);
        n1 = n2;
    }
    LIST_INIT(&mail->recipients);
}

void free_mail(Mail** _mail) {
    Mail* mail = *_mail;
    if (mail == NULL) return;

    _sfree(mail->from);
    _sfree(mail->data);
    _sfree(mail->x_original_to);

    StringEl *n2, *n1 = LIST_FIRST(&mail->recipients);
    while ( n1 != NULL ) {
        n2 = LIST_NEXT(n1, next);
        LIST_REMOVE(n1, next);
        _sfree(n1->content);
        free(n1);
        n1 = n2;
    }

    free(mail);
    *_mail = NULL;
}

int marshall_mail(const Mail* mail, const char* file) {
#define _append_utf8_field(doc, field, label)                                              \
    if ( !BSON_APPEND_UTF8(doc, #field, mail->field == NULL ? "" : mail->field) ) { \
        error("Error while appending '%s'", #field);                                \
        goto label;                                                                \
    }

    uint8_t *buf = NULL;
    size_t buflen = 0;
    bson_t *doc;

    bson_writer_t* writer = bson_writer_new(&buf, &buflen, 0, bson_realloc_ctx, NULL);
    bson_writer_begin(writer, &doc);

    _append_utf8_field(doc, from, _merror);
    _append_utf8_field(doc, data, _merror);
    _append_utf8_field(doc, x_original_to, _merror);

    {
        bson_t child;
        if ( !BSON_APPEND_ARRAY_BEGIN(doc, "recipients", &child) ) {
            error("Error while start appending array 'recipients'");
            goto _merror;
        }
        int _error_in_insert = 0;
        StringEl *n = LIST_FIRST(&mail->recipients);
        while ( !_error_in_insert && n != NULL ) {
            if ( BSON_APPEND_UTF8(&child, "", n->content) )
                n = LIST_NEXT(n, next);
            else {
                _error_in_insert = 1;
                error("Error while appending '%s' to bsont 'recipients'", n->content);
            }
        }

        if ( _error_in_insert ) goto _merror;

        if ( !bson_append_array_end(doc, &child) ) {
            error("Error while end appending array 'recipients'");
            goto _merror;
        }
    }

    bson_writer_end (writer);
    bson_writer_destroy(writer);

    FILE *fp;
    if ( !(fp=fopen(file, "w")) ) {
        error("Can't open file '%s' to write: %s", file, strerror(errno));
        bson_free(buf);
        return -errno;
    }
    size_t res = fwrite(buf, sizeof(uint8_t), buflen, fp);
    fclose(fp);
    bson_free(buf);

    return -(res != buflen);

_merror:
    bson_writer_destroy(writer);
    bson_free(buf);
    return -1;
}

int marshall_mail_to_string(const Mail* mail, char** custom_buffer) {
    uint8_t *buf = NULL;
    size_t buflen = 0;
    bson_t *doc;

    bson_writer_t* writer = bson_writer_new(&buf, &buflen, 0, bson_realloc_ctx, NULL);
    bson_writer_begin(writer, &doc);

    _append_utf8_field(doc, from, _merror);
    _append_utf8_field(doc, data, _merror);
    _append_utf8_field(doc, x_original_to, _merror);

    {
        bson_t child;
        if ( !BSON_APPEND_ARRAY_BEGIN(doc, "recipients", &child) ) {
            error("Error while start appending array 'recipients'");
            goto _merror;
        }
        int _error_in_insert = 0;
        StringEl *n = LIST_FIRST(&mail->recipients);
        while ( !_error_in_insert && n != NULL ) {
            if ( BSON_APPEND_UTF8(&child, "", n->content) )
                n = LIST_NEXT(n, next);
            else {
                _error_in_insert = 1;
                error("Error while appending '%s' to bsont 'recipients'", n->content);
            }
        }

        if ( _error_in_insert ) goto _merror;

        if ( !bson_append_array_end(doc, &child) ) {
            error("Error while end appending array 'recipients'");
            goto _merror;
        }
    }

    bson_writer_end (writer);
    bson_writer_destroy(writer);

    *custom_buffer = (char*)buf;
    return buflen * sizeof(uint8_t);

_merror:
    bson_writer_destroy(writer);
    bson_free(buf);
    return 0;
}

Mail* demarshall_mail(const char* file) {
#define _get_utf8_field(iter, field, result)                            \
    {                                                                   \
        if ( !bson_iter_find(&iter, #field) ) {                         \
            error("There is no field in '%s' like '%s'", file, #field); \
            goto _derror;                                               \
        }                                                               \
                                                                        \
        if ( bson_iter_type(&iter) != BSON_TYPE_UTF8 ) {                \
            error("Bad '%s' field in '%s'", #field, file);              \
            goto _derror;                                               \
        }                                                               \
                                                                        \
        uint32_t length;                                                \
        const char* value = bson_iter_utf8(&iter, &length);             \
                                                                        \
        result->field = malloc((length + 1) * sizeof(char));            \
        strcpy(result->field, value);                                   \
    }

    Mail* result = new_mail();
    bson_reader_t *reader;
    bson_error_t err;

    if (!(reader = bson_reader_new_from_file(file, &err))) {
        error("Failed to open '%s': %s", file, err.message);
        goto _derror;
    }

    const bson_t* mail = bson_reader_read(reader, NULL);
    bson_iter_t iter;
    if ( !mail || !bson_iter_init(&iter, mail) ) {
        error("Bad content or empty file '%s'", file);
        goto _derror;
    }

    _get_utf8_field(iter, from, result);
    _get_utf8_field(iter, data, result);
    _get_utf8_field(iter, x_original_to, result);

    {
        if ( !bson_iter_find(&iter, "recipients") ) {
            error("There is no field in '%s' like 'recipients'", file);
            goto _derror;
        }

        if ( bson_iter_type(&iter) != BSON_TYPE_ARRAY ) {
            error("Bad 'recipients' field ( we want array ) in '%s'", file);
            goto _derror;
        }

        bson_iter_t to;
        bson_iter_recurse(&iter, &to);

        StringEl* last = NULL;
        while ( bson_iter_next(&to) ) {
            if ( bson_iter_type(&to) != BSON_TYPE_UTF8 ) {
                error("Bad element type in 'recipients' field in '%s'", file);
                goto _derror;
            }
            StringEl* str = malloc(sizeof(StringEl));
            uint32_t length;
            const char* value = bson_iter_utf8(&to, &length);
            str->content = malloc((length + 1) * sizeof(char));
            strcpy(str->content, value);
            if ( last == NULL )
                LIST_INSERT_HEAD(&result->recipients, str, next);
            else {
                LIST_INSERT_AFTER(last, str, next);
            }
            last = str;
        }
    }


    bson_reader_destroy(reader);
    return result;

_derror:
    if ( reader ) bson_reader_destroy(reader);
    free_mail(&result);
    return NULL;
}