#!/usr/bin/ruby

require "mini-smtp-server"

require "test/unit"


class Consumer < MiniSmtpServer
  attr_accessor :has_some
  def new_message_event(message_hash)
	sleep 5
    @has_some += 1
  end
  def initialize
    super(2525, "127.0.0.1", 4)
    @has_some = 0
  end
end

class SlowTest < Test::Unit::TestCase
  def test_send
    server = Consumer.new
    server.audit = true
    
    server.start
    
    generator = File.expand_path(File.join(File.dirname(__FILE__), ".", "generate.rb"))
    
    maildir = "/tmp/#{Process.pid}"
    system("rm -rf #{maildir}; mkdir #{maildir}; mkdir #{maildir}/transit");
    system("#{generator} first@localhost second@localhost > #{maildir}/transit/mail");
    
    client = File.expand_path(File.join(File.dirname(__FILE__), "../..", "smtp_client"))
    system("#{client} -m #{maildir} -i 2 --timeout 2 -t 2 -l /tmp/log -p 2525");
	
	while(server.connections > 0)
		sleep 0.01
	end
	server.stop
	server.join
	
    assert(server.has_some == 1, "Exactly one message total")
    assert(File.exist?("#{maildir}/transit/mail"), "No removement!")
    assert_equal(`grep first@localhost #{maildir}/transit/mail && grep second@localhost #{maildir}/transit/mail`, "Binary file #{maildir}/transit/mail matches\nBinary file #{maildir}/transit/mail matches\n", "No changes")
  end
end