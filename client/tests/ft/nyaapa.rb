#!/usr/bin/ruby

require "test/unit"


class CrazyTest < Test::Unit::TestCase
  def test_send
    
    generator = File.expand_path(File.join(File.dirname(__FILE__), ".", "generate.rb"))
    
    maildir = "/tmp/#{Process.pid}"
    system("rm -rf #{maildir}; mkdir #{maildir}; mkdir #{maildir}/transit");
    system("#{generator} nyaapa@yandex.ru > #{maildir}/transit/mail");
    
    client = File.expand_path(File.join(File.dirname(__FILE__), "../..", "smtp_client"))
    system("#{client} -m #{maildir} -i 2 --timeout 2 -t 2 -l /tmp/log -p 25");
	
    assert(!File.exist?("#{maildir}/transit/mail"), "Remove mail file")
  end
end