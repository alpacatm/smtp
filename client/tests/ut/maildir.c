#include <check.h>
#include "maildir.h"
#include "mail.h"
#include <time.h>
#include <unistd.h>
#include "stdio.h"
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>

char* get_maildir() {
    return getenv("TMAILDIR");
}

START_TEST (check_maildir)
{
    char* maildir = get_maildir();
    ck_assert(maildir != NULL);
    char* new_dir_name = malloc((strlen(maildir) + sizeof("/transit")) * sizeof(char));
    strcpy(new_dir_name, maildir);
    strcat(new_dir_name, "/transit");
    DIR* new_dir = opendir(new_dir_name);
    free(new_dir_name);
    ck_assert(opendir(maildir) != NULL && new_dir != NULL);
}
END_TEST

START_TEST (unexisted_maildir)
{
    ck_assert(get_maildir_transfer("/lalala") == NULL);
}
END_TEST


START_TEST (check_tree)
{
    OutMail* res = get_maildir_transfer(get_maildir());
    ck_assert(res != NULL);
    int count = 0;
    DomainMail* next = NULL;
    for (DomainMail* rcpt = RB_MIN(OutMail, res); rcpt != NULL; rcpt = next) {
        next = RB_NEXT(OutMail, res, rcpt);
        count++;
    }
    ck_assert(count == 2);

    DomainMail tmp, *first, *second;
    tmp.domain = "first";
    ck_assert((first = RB_FIND(OutMail, res, &tmp)) != NULL);
    tmp.domain = "second";
    ck_assert((second = RB_FIND(OutMail, res, &tmp)) != NULL);

#define _check_count_in_list(list, await)   \
    {                                       \
        int count = 0;                      \
        MailEl* n = LIST_FIRST(list->mail); \
        while ( n != NULL ) {               \
            n = LIST_NEXT(n, next);         \
            count++;                        \
        }                                   \
        ck_assert(count == await);          \
    }

    _check_count_in_list(first, 2);
    _check_count_in_list(second, 1);

#define _check_existance_in_list(list, searching_for)           \
    {                                                           \
        const Mail* finded = NULL;                              \
        MailEl* n = LIST_FIRST(list->mail);                     \
        while ( n != NULL ) {                                   \
            if ( !strcmp(n->info->from, searching_for) ) {      \
                finded = n->info;                               \
                break;                                          \
            }                                                   \
            n = LIST_NEXT(n, next);                             \
        }                                                       \
        ck_assert(finded != NULL);                              \
    }

    _check_existance_in_list(first, "first_second");
    _check_existance_in_list(first, "first");
    _check_existance_in_list(second, "first_second");
}
END_TEST

int main(void) {
    Suite *s1 = suite_create("Maildir");
    TCase *tc1_1 = tcase_create("Maildir");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, check_maildir);
    tcase_add_test(tc1_1, unexisted_maildir);
    tcase_add_test(tc1_1, check_tree);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf != 0;
}