#define _GNU_SOURCE

#include <check.h>
#include "taskqueue.h"
#include <time.h>
#include <unistd.h>
#include "stdio.h"
#include <errno.h>

void* getter_thread(void* queue) {
    return get_task(queue);
}

START_TEST (add_get)
{
    TaskQueue* queue = new_queue();
    add_task(queue, (void*) 42);
    ck_assert_int_eq((int) get_task(queue), 42);
}
END_TEST

START_TEST (get_add)
{
    TaskQueue* queue = new_queue();
    pthread_t thread;
    pthread_create(&thread, NULL, &getter_thread, queue);
    struct timespec ts;
    void* res;
    ts.tv_sec = 2;
    ts.tv_nsec = 0;
    ck_assert_int_eq(pthread_timedjoin_np(thread, &res, &ts), ETIMEDOUT);
    add_task(queue, (void*) 42);
    pthread_join(thread, &res);
    ck_assert_int_eq((int) res, 42);
}
END_TEST

START_TEST (some_queue)
{
    const int tasks_count = 10;
    const int threads_count = tasks_count * 2;

    int result[tasks_count];
    pthread_t threads[threads_count];

    TaskQueue* queue = new_queue();

    for ( int i = 0; i < threads_count; i++ )
        pthread_create(&threads[i], NULL, &getter_thread, queue);

    for ( long i = 0; i < tasks_count; i++ ) {
        result[i] = 0;
        add_task(queue, (void*) i);
    }

    for ( int i = 0; i < threads_count; i++ ) 
        add_task(queue, (void*) -1);

    for ( int i = 0; i < threads_count; i++ ) {
        void* res;
        pthread_join(threads[i], &res);
        if ( (int) res != -1 ) {
            ck_assert((int) res >= 0 && (int) res < tasks_count);
            result[(int) res]++;
        }
    }
    for ( int i = 0; i < tasks_count; i++ )
        ck_assert_int_eq(result[i], 1);
}
END_TEST


int main(void) {
    Suite *s1 = suite_create("Taskqueue");
    TCase *tc1_1 = tcase_create("Taskqueue");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, add_get);
    tcase_add_test(tc1_1, get_add);
    tcase_add_test(tc1_1, some_queue);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf != 0;
}