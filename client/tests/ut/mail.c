#include <check.h>
#include "mail.h"
#include <unistd.h>
#include "stdio.h"
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

char* get_mail_dir() {
    char* maildir = getenv("TESTDIR");
    char* new_dir_name = malloc((strlen(maildir) + sizeof("/mail") + 1) * sizeof(char));
    strcpy(new_dir_name, maildir);
    strcat(new_dir_name, "/mail");
    return new_dir_name;
}

char* random_fname() {
    int pid = getpid();
    int utime = time(NULL);
    char* fname = NULL;
    do {
        if ( fname ) free(fname);
        int randn = rand();
        fname = malloc((snprintf(NULL, 0, "/tmp/.%d.%d.%d", pid, utime, randn) + 1) * sizeof(char));
        sprintf(fname, "/tmp/.%d.%d.%d", pid, utime, randn);
    } while ( access(fname, F_OK) != -1 );
    return fname;
}

START_TEST (check_in_out)
{
#define _set_string_field(mail, field)                              \
    {                                                               \
        mail->field = malloc((strlen(#field) + 1) * sizeof(char));  \
        strcpy(mail->field, #field);                                \
    }
#define _compare_string_field(one, two, field, ptr) {   \
        ck_assert_str_eq(one->field, two->field);       \
        ptr += sizeof(char*);                           \
    }
    Mail* mail = new_mail();

    _set_string_field(mail, from);
    _set_string_field(mail, data);
    _set_string_field(mail, x_original_to);

    {
        for ( int i = 0; i < 5; i++ ) {
            StringEl* el = malloc(sizeof(StringEl));
            el->content = malloc(
                (strlen("recipients") + 1) *
                sizeof(char)
            );
            strcpy(el->content, "recipients");
            sprintf(el->content + strlen("recipients"), "%d", i);
            LIST_INSERT_HEAD(&mail->recipients, el, next);
        }
    }

    char* fname = random_fname();
    ck_assert(!marshall_mail(mail, fname));
    Mail* mail2 = demarshall_mail(fname);
    ck_assert(mail2 != NULL);
    free(fname);

    uint32_t shift = 0;

    // trace
    _compare_string_field(mail, mail2, from, shift);
    _compare_string_field(mail, mail2, data, shift);
    _compare_string_field(mail, mail2, x_original_to, shift);

    {
        StringEl *first = LIST_FIRST(&mail->recipients), *second = LIST_FIRST(&mail2->recipients);
        while ( first != NULL && second != NULL ) {
            ck_assert_str_eq(first->content, second->content);
            first = LIST_NEXT(first, next); second = LIST_NEXT(second, next);
        }
        ck_assert(first == NULL && second == NULL);
        shift += sizeof(StringList);
    }

    free_mail(&mail);
    free_mail(&mail2);
    ck_assert_int_eq(shift, sizeof(Mail));
}
END_TEST


START_TEST (check_empty_fields)
{
    Mail* mail = new_mail();
    char* fname = random_fname();
    ck_assert(!marshall_mail(mail, fname));
    Mail* mail2 = demarshall_mail(fname);
    ck_assert(mail2 != NULL);
    free(fname);
    ck_assert_str_eq(mail2->data, "");
    ck_assert(mail->data == NULL);
    free_mail(&mail);
    free_mail(&mail2);
}
END_TEST

START_TEST (unexisted_file)
{
    ck_assert(demarshall_mail("/tmp") == NULL);
    char* fname = random_fname();
    ck_assert(demarshall_mail(fname) == NULL);
}
END_TEST

START_TEST (cant_save)
{
    Mail* mail = new_mail();
    ck_assert(marshall_mail(mail, "/tmp"));
}
END_TEST

START_TEST (unexisted_field)
{
#define _yam(dest, part1, part2)                                            \
    {                                                                       \
        if ( dest ) free(dest);                                             \
        dest = malloc((strlen(part1) + strlen(part2) + 2) * sizeof(char));  \
        sprintf(dest, "%s/%s", part1, part2);                               \
    }
    char* tdir = get_mail_dir();
    char* tmp = NULL;
    _yam(tmp, tdir, "no_recipients");
    ck_assert(demarshall_mail(tmp) == NULL);
    _yam(tmp, tdir, "no_data");
    ck_assert(demarshall_mail(tmp) == NULL);
    free(tdir);
}
END_TEST

START_TEST (bad_field)
{
#define _yam(dest, part1, part2)                                            \
    {                                                                       \
        if ( dest ) free(dest);                                             \
        dest = malloc((strlen(part1) + strlen(part2) + 2) * sizeof(char));  \
        sprintf(dest, "%s/%s", part1, part2);                               \
    }
    char* tdir = get_mail_dir();
    char* tmp = NULL;
    _yam(tmp, tdir, "bad_data");
    ck_assert(demarshall_mail(tmp) == NULL);
    _yam(tmp, tdir, "bad_recipients");
    ck_assert(demarshall_mail(tmp) == NULL);
    _yam(tmp, tdir, "bad_one_of_recipients");
    ck_assert(demarshall_mail(tmp) == NULL);
    free(tdir);
}
END_TEST

START_TEST (empty_file)
{
    char* fname = random_fname();
    FILE *fh = fopen(fname, "w");
    fclose(fh);
    ck_assert(demarshall_mail(fname) == NULL);
}
END_TEST


int main(void) {
    srand(time(NULL));

    Suite *s1 = suite_create("Mail");
    TCase *tc1_1 = tcase_create("Mail");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, check_in_out);
    tcase_add_test(tc1_1, check_empty_fields);
    tcase_add_test(tc1_1, unexisted_file);
    tcase_add_test(tc1_1, cant_save);
    tcase_add_test(tc1_1, unexisted_field);
    tcase_add_test(tc1_1, bad_field);
    tcase_add_test(tc1_1, empty_file);


    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf != 0;
}