#include "client-cmd.h"

#include <string.h>
#include <stdlib.h>


#include "client-run.h"
#include "client-re.h"
#include "hostutil.h"
#include "logger.h"

te_client_fsm_state client_cmd_ehlo(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    te_client_fsm_state res = state->state;
    char* hostname = get_hostname();
    char* msg = malloc((7 + strlen(hostname) + 1) * sizeof(char));
    strcpy(msg, "helo ");
    strcat(msg, hostname);
    strcat(msg, "\r\n");
    free(hostname);
    response_status response = get_response_status(connection, msg, timeout);
    if ( response.status == rtimeout ) {
        error("%s timeouted", msg);
        *ev = CLIENT_FSM_EV_TIMEOUT;
    } else if ( response.status < rbad ) {
        *ev = CLIENT_FSM_EV_CMD_MAIL;
        res = CLIENT_FSM_ST_EHLO;
    } else {
        error("There was an error for %s: %d", msg, response.smtp_code);
        *ev = CLIENT_FSM_EV_CMD_ERROR;
    }
    free(msg);
    return res;
}

te_client_fsm_state client_cmd_mail(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    te_client_fsm_state res = state->state;
    char* msg = malloc((strlen("mail from: <>\r\n") + strlen(mail->from) + 1) * sizeof(char));
    strcpy(msg, "mail from: <");
    strcat(msg, mail->from);
    strcat(msg, ">\r\n");
    response_status response = get_response_status(connection, msg, timeout);
    if ( response.status == rtimeout ) {
        error("%s timeouted", msg);
        *ev = CLIENT_FSM_EV_TIMEOUT;
    } else if ( response.status < rbad ) {
        *ev = CLIENT_FSM_EV_CMD_RCPT;
        res = CLIENT_FSM_ST_MAIL;
    } else {
        error("There was an error for %s: %d", msg, response.smtp_code);
        *ev = CLIENT_FSM_EV_CMD_ERROR;
    }
    free(msg);
    return res;
}

te_client_fsm_state client_cmd_init_rcpt(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    state->rcpt = LIST_FIRST(&mail->recipients);
    *ev = CLIENT_FSM_EV_CMD_RCPT;
    return CLIENT_FSM_ST_RCPT;
}


te_client_fsm_state client_cmd_yar(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    te_client_fsm_state res = state->state;
    if ( state->rcpt != NULL ) {
        char* msg = malloc((strlen("rcpt to: <>\r\n") + strlen(state->rcpt->content) + 1) * sizeof(char));
        strcpy(msg, "rcpt to: <");
        strcat(msg, state->rcpt->content);
        strcat(msg, ">\r\n");
        response_status response = get_response_status(connection, msg, timeout);
        if ( response.status == rtimeout ) {
            error("%s timeouted", msg);
            *ev = CLIENT_FSM_EV_TIMEOUT;
        } else if ( response.status < rbad ) {
            *ev = CLIENT_FSM_EV_CMD_RCPT;
            res = CLIENT_FSM_ST_RCPT;
        } else {
            error("There was an error for %s: %d", msg, response.smtp_code);
            *ev = CLIENT_FSM_EV_CMD_ERROR;
        }
        free(msg);

        state->rcpt = LIST_NEXT(state->rcpt, next);
    } else {
        *ev = CLIENT_FSM_EV_CMD_DATA;
        res = CLIENT_FSM_ST_RCPT;
    }
    return res;
}

te_client_fsm_state client_cmd_init_data(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    te_client_fsm_state res = state->state;
    char* msg = malloc((strlen("data\r\n") + 1) * sizeof(char));
    strcpy(msg, "data\r\n");
    response_status response = get_response_status(connection, msg, timeout);
    if ( response.status == rtimeout ) {
        error("%s timeouted", msg);
        *ev = CLIENT_FSM_EV_TIMEOUT;
    } else if ( response.status < rbad ) {
        *ev = CLIENT_FSM_EV_CMD_DATA;
        res = CLIENT_FSM_ST_DATA;
    } else {
        error("There was an error for %s: %d", msg, response.smtp_code);
        *ev = CLIENT_FSM_EV_CMD_ERROR;
    }
    free(msg);
    return res;
}

te_client_fsm_state client_cmd_data(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    te_client_fsm_state res = state->state;
    size_t data_length = strlen(mail->data);
    char* msg = malloc((strlen("\r\n.\r\n") + data_length + 1) * sizeof(char));
    strcpy(msg, mail->data);
    if (
        data_length > 2
        && (mail->data[data_length-1] != '\n' || mail->data[data_length-2] != '\r')
    )
        strcat(msg, "\r\n");
    strcat(msg, ".\r\n");
    response_status response = get_response_status(connection, msg, timeout);
    if ( response.status == rtimeout ) {
        error("%s timeouted", msg);
        *ev = CLIENT_FSM_EV_TIMEOUT;
    } else if ( response.status < rbad ) {
        res = CLIENT_FSM_ST_DONE;
    } else {
        error("There was an error for %s: %d", msg, response.smtp_code);
        *ev = CLIENT_FSM_EV_CMD_ERROR;
    }
    free(msg);
    return res;
}

te_client_fsm_state client_cmd_error(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    *ev = CLIENT_FSM_EV_CMD_ERROR;
    return CLIENT_FSM_ST_DONE;
}

te_client_fsm_state client_cmd_timeout(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev) {
    *ev = CLIENT_FSM_EV_TIMEOUT;
    return CLIENT_FSM_ST_DONE;
}