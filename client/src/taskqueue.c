#include "taskqueue.h"
#include "stdlib.h"
#include "stdio.h"

TaskQueue* new_queue() {
    struct TaskQueue *queue = malloc(sizeof(TaskQueue));
    pthread_mutex_init(&queue->lock, NULL);
    pthread_cond_init(&queue->has_some, NULL);
    queue->tasks = malloc(sizeof(TaskHead));
    TAILQ_INIT(queue->tasks);
    return queue;
}

void add_task(TaskQueue* queue, void* task) {
    pthread_mutex_lock(&queue->lock);
    TaskEl* el = malloc(sizeof(TaskEl));
    el->content = task;

    if ( TAILQ_EMPTY(queue->tasks) )
        pthread_cond_signal(&queue->has_some);
    TAILQ_INSERT_TAIL(queue->tasks, el, next);

    pthread_mutex_unlock(&queue->lock);
}

void* get_task(TaskQueue* queue) {
    pthread_mutex_lock(&queue->lock);

    while ( TAILQ_EMPTY(queue->tasks) )
        pthread_cond_wait(&queue->has_some, &queue->lock);

    TaskEl* el = TAILQ_FIRST(queue->tasks);
    TAILQ_REMOVE(queue->tasks, el, next);

    pthread_cond_signal(&queue->has_some);
    pthread_mutex_unlock(&queue->lock);

    void* result = el->content;
    free(el);
    return result;
}
