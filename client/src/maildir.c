#include "maildir.h"
#include "logger.h"
#include "mail.h"
#include "hostutil.h"

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <pcre.h>

typedef struct _Rcpt {
    char* recp;
    RB_ENTRY(_Rcpt) entry;
} _Rcpt;

typedef RB_HEAD(_Dest, _Rcpt) _Dest;

int _cmp_recps(const _Rcpt *a, const _Rcpt *b);

_Dest _get_destinations(char* mailpath, const Mail* mail);

RB_GENERATE(OutMail, DomainMail, entry, _cmp_domains)
RB_GENERATE(_Dest, _Rcpt, entry, _cmp_recps)

OutMail* get_maildir_transfer(const char* dir) {
    char* transit_dir_name = malloc((strlen(dir) + strlen("/transit") + 1) * sizeof(char));
    strcpy(transit_dir_name, dir);
    strcat(transit_dir_name, "/transit");
    DIR* transit_dir = opendir(transit_dir_name);
    if ( transit_dir == NULL ) {
        error("Can't open %s': %s", transit_dir_name, strerror(errno));
        free(transit_dir_name);
        return NULL;
    }

    OutMail head = RB_INITIALIZER(head);

    struct dirent *dp;
    while ((dp = readdir (transit_dir)) != NULL) {
        if ( dp->d_name[0] == '.' ) continue;

        char* fullname = malloc((strlen(transit_dir_name) + strlen(dp->d_name) + 2) * sizeof(char));
        strcpy(fullname, transit_dir_name);
        strcat(fullname, "/");
        strcat(fullname, dp->d_name);

        Mail* mail = demarshall_mail(fullname);
        if ( !mail ) {
            error("Failed to parse '%s'", fullname);
            free(fullname);
            continue;
        }

        _Dest receivers = _get_destinations(fullname, mail);
        _Rcpt* next;
        int inserted = 0;
        for (_Rcpt* rcpt = RB_MIN(_Dest, &receivers); rcpt != NULL; rcpt = next) {
            next = RB_NEXT(_Dest, &receivers, rcpt);
            RB_REMOVE(_Dest, &receivers, rcpt);

            DomainMail* domain = malloc(sizeof(DomainMail));
            domain->domain = rcpt->recp;
            free(rcpt);

            DomainMail* existed;
            if ( (existed = RB_FIND(OutMail, &head, domain)) ) {
                free(domain->domain);
                free(domain);
                domain = existed;
            } else {
                RB_INSERT(OutMail, &head, domain);
                domain->mail = malloc(sizeof(MailsHead));
                LIST_INIT(domain->mail);
            }

            MailEl* dmail = malloc(sizeof(MailEl));
            dmail->info = mail;
            dmail->path = fullname;
            LIST_INSERT_HEAD(domain->mail, dmail, next);
            inserted = 1;
        }

        if ( !inserted ) free(fullname);
    }

    free(transit_dir_name);

    // Don't know how to work with tree as ref
    OutMail* result = malloc(sizeof(OutMail));
    *result = head;
    return result;
}


int _cmp_domains(const DomainMail *a, const DomainMail *b) {
    return strcmp(a->domain, b->domain);
}

int _cmp_recps(const _Rcpt *a, const _Rcpt *b) {
    return strcmp(a->recp, b->recp);
}

_Dest _get_destinations(char* mailpath, const Mail* mail) {
    _Dest receivers = RB_INITIALIZER(&receivers);

    StringEl* n = LIST_FIRST(&mail->recipients);
    while ( n != NULL ) {
        char* domain = get_email_domain(n->content);
        if ( domain == NULL ) {
            error("Bad destination '%s' in 'recipients'", n->content);
            return receivers;
        }
        free(domain);
        n = LIST_NEXT(n, next);
    }

    n = LIST_FIRST(&mail->recipients);
    while ( n != NULL ) {
        _Rcpt* recp = malloc(sizeof(_Rcpt));
        recp->recp = get_email_domain(n->content);

        RB_INSERT(_Dest, &receivers, recp);

        n = LIST_NEXT(n, next);
    }

    return receivers;
}