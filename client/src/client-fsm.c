/*   -*- buffer-read-only: t -*- vi: set ro:
 *
 *  DO NOT EDIT THIS FILE   (client-fsm.c)
 *
 *  It has been AutoGen-ed  December 18, 2014 at 05:16:22 AM by AutoGen 5.18.3
 *  From the definitions    client.def
 *  and the template file   fsm
 *
 *  Automated Finite State Machine
 *
 *  Copyright (C) 1992-2014 Bruce Korb - all rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``Bruce Korb'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * AutoFSM IS PROVIDED BY Bruce Korb ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL Bruce Korb OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#define DEFINE_FSM
#include "client-fsm.h"
#include <stdio.h>

/*
 *  Do not make changes to this file, except between the START/END
 *  comments, or it will be removed the next time it is generated.
 */
/* START === USER HEADERS === DO NOT CHANGE THIS COMMENT */
#include "client-cmd.h"
#include "logger.h"
/* END   === USER HEADERS === DO NOT CHANGE THIS COMMENT */

#ifndef NULL
#  define NULL 0
#endif

/**
 *  Enumeration of the valid transition types
 *  Some transition types may be common to several transitions.
 */
typedef enum {
    CLIENT_FSM_TR_DATA_CMD_DATA,
    CLIENT_FSM_TR_DATA_CMD_ERROR,
    CLIENT_FSM_TR_DATA_TIMEOUT,
    CLIENT_FSM_TR_EHLO_CMD_ERROR,
    CLIENT_FSM_TR_EHLO_CMD_MAIL,
    CLIENT_FSM_TR_EHLO_TIMEOUT,
    CLIENT_FSM_TR_INIT_CMD_EHLO,
    CLIENT_FSM_TR_INIT_CMD_ERROR,
    CLIENT_FSM_TR_INIT_TIMEOUT,
    CLIENT_FSM_TR_INVALID,
    CLIENT_FSM_TR_MAIL_CMD_ERROR,
    CLIENT_FSM_TR_MAIL_CMD_RCPT,
    CLIENT_FSM_TR_MAIL_TIMEOUT,
    CLIENT_FSM_TR_RCPT_CMD_DATA,
    CLIENT_FSM_TR_RCPT_CMD_ERROR,
    CLIENT_FSM_TR_RCPT_CMD_RCPT,
    CLIENT_FSM_TR_RCPT_TIMEOUT
} te_client_fsm_trans;
#define CLIENT_FSM_TRANSITION_CT  17

/**
 *  State transition handling map.  Map the state enumeration and the event
 *  enumeration to the new state and the transition enumeration code (in that
 *  order).  It is indexed by first the current state and then the event code.
 */
typedef struct client_fsm_transition t_client_fsm_transition;
struct client_fsm_transition {
    te_client_fsm_state  next_state;
    te_client_fsm_trans  transition;
};
static const t_client_fsm_transition
client_fsm_trans_table[ CLIENT_FSM_STATE_CT ][ CLIENT_FSM_EVENT_CT ] = {

    /* STATE 0:  CLIENT_FSM_ST_INIT */
    {   { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_INIT_TIMEOUT }, /* EVT:  TIMEOUT */
        { CLIENT_FSM_ST_EHLO, CLIENT_FSM_TR_INIT_CMD_EHLO }, /* EVT:  CMD_EHLO */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_MAIL */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_RCPT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_DATA */
        { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_INIT_CMD_ERROR } /* EVT:  CMD_ERROR */
    },


    /* STATE 1:  CLIENT_FSM_ST_EHLO */
    {   { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_EHLO_TIMEOUT }, /* EVT:  TIMEOUT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_EHLO */
        { CLIENT_FSM_ST_MAIL, CLIENT_FSM_TR_EHLO_CMD_MAIL }, /* EVT:  CMD_MAIL */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_RCPT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_DATA */
        { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_EHLO_CMD_ERROR } /* EVT:  CMD_ERROR */
    },


    /* STATE 2:  CLIENT_FSM_ST_MAIL */
    {   { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_MAIL_TIMEOUT }, /* EVT:  TIMEOUT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_EHLO */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_MAIL */
        { CLIENT_FSM_ST_RCPT, CLIENT_FSM_TR_MAIL_CMD_RCPT }, /* EVT:  CMD_RCPT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_DATA */
        { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_MAIL_CMD_ERROR } /* EVT:  CMD_ERROR */
    },


    /* STATE 3:  CLIENT_FSM_ST_RCPT */
    {   { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_RCPT_TIMEOUT }, /* EVT:  TIMEOUT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_EHLO */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_MAIL */
        { CLIENT_FSM_ST_RCPT, CLIENT_FSM_TR_RCPT_CMD_RCPT }, /* EVT:  CMD_RCPT */
        { CLIENT_FSM_ST_DATA, CLIENT_FSM_TR_RCPT_CMD_DATA }, /* EVT:  CMD_DATA */
        { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_RCPT_CMD_ERROR } /* EVT:  CMD_ERROR */
    },


    /* STATE 4:  CLIENT_FSM_ST_DATA */
    {   { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_DATA_TIMEOUT }, /* EVT:  TIMEOUT */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_EHLO */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_MAIL */
        { CLIENT_FSM_ST_INVALID, CLIENT_FSM_TR_INVALID }, /* EVT:  CMD_RCPT */
        { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_DATA_CMD_DATA }, /* EVT:  CMD_DATA */
        { CLIENT_FSM_ST_DONE, CLIENT_FSM_TR_DATA_CMD_ERROR } /* EVT:  CMD_ERROR */
    }
};


#define Client_FsmFsmErr_off     19
#define Client_FsmEvInvalid_off  75
#define Client_FsmStInit_off     83


static char const zClient_FsmStrings[162] =
    /*     0 */ "** OUT-OF-RANGE **\0"
    /*    19 */ "FSM Error:  in state %d (%s), event %d (%s) is invalid\n\0"
    /*    75 */ "invalid\0"
    /*    83 */ "init\0"
    /*    88 */ "ehlo\0"
    /*    93 */ "mail\0"
    /*    98 */ "rcpt\0"
    /*   103 */ "data\0"
    /*   108 */ "timeout\0"
    /*   116 */ "cmd_ehlo\0"
    /*   125 */ "cmd_mail\0"
    /*   134 */ "cmd_rcpt\0"
    /*   143 */ "cmd_data\0"
    /*   152 */ "cmd_error";

static const size_t aszClient_FsmStates[5] = {
    83,  88,  93,  98,  103
};

static const size_t aszClient_FsmEvents[7] = {
    108, 116, 125, 134, 143, 152, 75
};


#define CLIENT_FSM_EVT_NAME(t)   ( (((unsigned)(t)) >= 7) \
    ? zClient_FsmStrings : zClient_FsmStrings + aszClient_FsmEvents[t])

#define CLIENT_FSM_STATE_NAME(s) ( (((unsigned)(s)) >= 5) \
    ? zClient_FsmStrings : zClient_FsmStrings + aszClient_FsmStates[s])

#ifndef EXIT_FAILURE
# define EXIT_FAILURE 1
#endif

static int client_fsm_invalid_transition( te_client_fsm_state st, te_client_fsm_event evt );

/* * * * * * * * * THE CODE STARTS HERE * * * * * * * */
/**
 *  Print out an invalid transition message and return EXIT_FAILURE
 */
static int
client_fsm_invalid_transition( te_client_fsm_state st, te_client_fsm_event evt )
{
    /* START == INVALID TRANS MSG == DO NOT CHANGE THIS COMMENT */
    char const * fmt = zClient_FsmStrings + Client_FsmFsmErr_off;
    fprintf( stderr, fmt, st, CLIENT_FSM_STATE_NAME(st), evt, CLIENT_FSM_EVT_NAME(evt));
    /* END   == INVALID TRANS MSG == DO NOT CHANGE THIS COMMENT */

    return EXIT_FAILURE;
}

/**
 *  Step the FSM.  Returns the resulting state.  If the current state is
 *  CLIENT_FSM_ST_DONE or CLIENT_FSM_ST_INVALID, it resets to
 *  CLIENT_FSM_ST_INIT and returns CLIENT_FSM_ST_INIT.
 */
te_client_fsm_state
client_fsm_step(
    te_client_fsm_state client_fsm_state,
    te_client_fsm_event trans_evt,
    int connection,
    void *cmd,
    void *state,
    size_t timeout,
    te_client_fsm_event* ev )
{
    te_client_fsm_state nxtSt;
    te_client_fsm_trans trans;

    if ((unsigned)client_fsm_state >= CLIENT_FSM_ST_INVALID) {
        return CLIENT_FSM_ST_INIT;
    }

#ifndef __COVERITY__
    if (trans_evt >= CLIENT_FSM_EV_INVALID) {
        nxtSt = CLIENT_FSM_ST_INVALID;
        trans = CLIENT_FSM_TR_INVALID;
    } else
#endif /* __COVERITY__ */
    {
        const t_client_fsm_transition* pTT =
            client_fsm_trans_table[ client_fsm_state ] + trans_evt;
        nxtSt = pTT->next_state;
        trans = pTT->transition;
    }


    switch (trans) {
    case CLIENT_FSM_TR_DATA_CMD_DATA:
        /* START == DATA_CMD_DATA == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_data(connection, cmd, state, timeout, ev);
        /* END   == DATA_CMD_DATA == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_DATA_CMD_ERROR:
        /* START == DATA_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_error(connection, cmd, state, timeout, ev);
        /* END   == DATA_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_DATA_TIMEOUT:
        /* START == DATA_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_timeout(connection, cmd, state, timeout, ev);
        /* END   == DATA_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_EHLO_CMD_ERROR:
        /* START == EHLO_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_error(connection, cmd, state, timeout, ev);
        /* END   == EHLO_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_EHLO_CMD_MAIL:
        /* START == EHLO_CMD_MAIL == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_mail(connection, cmd, state, timeout, ev);
        /* END   == EHLO_CMD_MAIL == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_EHLO_TIMEOUT:
        /* START == EHLO_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_timeout(connection, cmd, state, timeout, ev);
        /* END   == EHLO_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_INIT_CMD_EHLO:
        /* START == INIT_CMD_EHLO == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_ehlo(connection, cmd, state, timeout, ev);
        /* END   == INIT_CMD_EHLO == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_INIT_CMD_ERROR:
        /* START == INIT_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_error(connection, cmd, state, timeout, ev);
        /* END   == INIT_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_INIT_TIMEOUT:
        /* START == INIT_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_timeout(connection, cmd, state, timeout, ev);
        /* END   == INIT_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_INVALID:
        /* START == INVALID == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_error(connection, cmd, state, timeout, ev);
        /* END   == INVALID == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_MAIL_CMD_ERROR:
        /* START == MAIL_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_error(connection, cmd, state, timeout, ev);
        /* END   == MAIL_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_MAIL_CMD_RCPT:
        /* START == MAIL_CMD_RCPT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_init_rcpt(connection, cmd, state, timeout, ev);
        /* END   == MAIL_CMD_RCPT == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_MAIL_TIMEOUT:
        /* START == MAIL_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_timeout(connection, cmd, state, timeout, ev);
        /* END   == MAIL_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_RCPT_CMD_DATA:
        /* START == RCPT_CMD_DATA == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_init_data(connection, cmd, state, timeout, ev);
        /* END   == RCPT_CMD_DATA == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_RCPT_CMD_ERROR:
        /* START == RCPT_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_error(connection, cmd, state, timeout, ev);
        /* END   == RCPT_CMD_ERROR == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_RCPT_CMD_RCPT:
        /* START == RCPT_CMD_RCPT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_yar(connection, cmd, state, timeout, ev);
        /* END   == RCPT_CMD_RCPT == DO NOT CHANGE THIS COMMENT */
        break;


    case CLIENT_FSM_TR_RCPT_TIMEOUT:
        /* START == RCPT_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        nxtSt = client_cmd_timeout(connection, cmd, state, timeout, ev);
        /* END   == RCPT_TIMEOUT == DO NOT CHANGE THIS COMMENT */
        break;


    default:
        /* START == BROKEN MACHINE == DO NOT CHANGE THIS COMMENT */
        error("Invalid state!");
        *ev = CLIENT_FSM_EV_CMD_ERROR;
        nxtSt = CLIENT_FSM_ST_INVALID;
        client_fsm_invalid_transition(client_fsm_state, trans_evt);
        /* END   == BROKEN MACHINE == DO NOT CHANGE THIS COMMENT */
    }


    /* START == FINISH STEP == DO NOT CHANGE THIS COMMENT */
    /* END   == FINISH STEP == DO NOT CHANGE THIS COMMENT */

    return nxtSt;
}
/*
 * Local Variables:
 * mode: C
 * c-file-style: "stroustrup"
 * indent-tabs-mode: nil
 * End:
 * end of client-fsm.c */
