/** \file client-run.c
*  \brief Основные функции.
*
*   В этом файле описана главная функция программы.
*/


#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <poll.h>
#include <stdlib.h>
#include <unistd.h>

#include "client-run.h"
#include "mx-state.h"
#include "client-cmd.h"
#include "client-re.h"
#include "hostutil.h"
#include "logger.h"


#define MAXCMDLEN 1024

int run(int connection, Mail* mail, size_t timeout) {
    int result = 0, loop = 1;
    te_client_fsm_event event = CLIENT_FSM_EV_CMD_EHLO;
    mx_state state;

    init_state(&state);
    while ( loop ) {
        state.state = client_fsm_step(state.state, event, connection, mail, &state, timeout, &event);

        if ( event == CLIENT_FSM_EV_CMD_ERROR || event == CLIENT_FSM_EV_TIMEOUT )
            result = -1;
        if ( state.state == CLIENT_FSM_ST_INVALID ) {
            result = -1;
            loop = 0;
        } else if ( state.state == CLIENT_FSM_ST_DONE ) {
            loop = 0;
        }
    }

    free_state(&state);
    return result;
}


response_status get_response_status(int connection, const char* cmd, size_t timeout) {
    response_status result;

    struct pollfd fds[1];
    fds[0].fd = connection;
    if ( cmd != NULL ) {
        debug("Send '%s'", cmd);
        fds[0].events = POLLWRNORM;

        if ( !poll(fds, 1, timeout * 1000) ) {
            info("At write");
            result.status = rtimeout;
            return result;
        }
        dprintf(connection, "%s", cmd);
    }

    fds[0].events = POLLRDNORM;
    if ( !poll(fds, 1, timeout * 1000) ) {
        info("%d", timeout);
        result.status = rtimeout;
        return result;
    }

    char buf[MAXCMDLEN];
    size_t res_size = read(connection, buf, MAXCMDLEN);
    buf[res_size] = '\0';
    debug("Get '%s'", buf);

    result.smtp_code = -1;
    result.status = rbad;
    int ovector[6];
    for ( int i = 0; i < rtimeout; i++ ) {
        int match;
        if ( (match = pcre_exec(smtp_re[i], NULL, buf, res_size, 0, PCRE_NEWLINE_ANY, ovector, 6)) == 2 ) {
            result.status = i;

            const char *sp = NULL;
            pcre_get_substring((const char *)buf, ovector, match, 1, &sp);
            result.smtp_code = atoi(sp);
            pcre_free_substring(sp);

            break;
        }
    }

    return result;
}

/* Косяк */
void __attribute__((constructor)) initre() {
    for ( int i = 0; i < _smt_re_count; i++ ) {
        const char *errstr;
        int errchar;
        if ( !((smtp_re[i] = pcre_compile(smtp_patterns[i], PCRE_UTF8 | PCRE_MULTILINE | PCRE_DOTALL, &errstr, &errchar, NULL))) ) {
            error("Error while compile rx %s at %d", errstr, errchar);
            assert(0);
        }
    }
}