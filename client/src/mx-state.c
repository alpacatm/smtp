#include <stdlib.h>

#include "mx-state.h"

void init_state(mx_state *state) {
    state->state = CLIENT_FSM_ST_INIT;
    state->rcpt = NULL;
}

void free_state(mx_state *state) {
    init_state(state);
}
