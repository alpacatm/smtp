/** \file client.c
 *  \brief Основные функции.
 *
 *   В этом файле описана главная функция программы.
 */


#include <stdio.h>
#include <string.h>


#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "checkoptn.h"

#include "logger.h"
#include "taskqueue.h"
#include "stdlib.h"
#include "maildir.h"
#include "hostutil.h"
#include "client-run.h"
#include "client-re.h"
/** \fn void* _process_domain(void* queue)
*  \brief Логика работа нити клиента.
*  \param queue -- очередь заданий посылки.
*  \return ничего.
*/
void* _process_domain(void* queue);

/** \fn int main(int argc, char *argv[])
 *  \brief Главная функция программы.
 *  \param argc -- число аргументов при вызове программы;
 *  \param argv -- аргументы вызова программы.
 *  \return код завершения программы.
 */
int main(int argc, char *argv[]) {
    int optct = optionProcess(&smtp_clientOptions, argc, argv);
    init_logger(OPT_ARG(LOG));
    argc -= optct;
    argv += optct;
    info("Start client for maildir=%s threads=%lu repeat_interval=%lu timeout=%lu port=%lu retries=%lu",
         OPT_ARG(MAILDIR),
         OPT_VALUE_THREAD,
         OPT_VALUE_INTERVAL,
         OPT_VALUE_TIMEOUT,
         OPT_VALUE_PORT,
         OPT_VALUE_RETRIES
        );

    OutMail* mail = get_maildir_transfer(OPT_ARG(MAILDIR));
    if ( !mail ) return -1;

    TaskQueue* queue = new_queue();

    pthread_t* threads = malloc(sizeof(pthread_t) * OPT_VALUE_THREAD);
    for ( int i = 0; i < OPT_VALUE_THREAD; i++ )
        pthread_create(&threads[i], NULL, &_process_domain, queue);

    DomainMail* next;
    for (DomainMail* rcpt = RB_MIN(OutMail, mail); rcpt != NULL; rcpt = next) {
        next = RB_NEXT(OutMail, mail, rcpt);
        add_task(queue, rcpt);
    }

    for ( int i = 0; i < OPT_VALUE_THREAD; i++ )
        add_task(queue, NULL);

    for ( int i = 0; i < OPT_VALUE_THREAD; i++ ) {
        int res;
        pthread_join(threads[i], (void*) &res);
    }

    for (DomainMail* rcpt = RB_MIN(OutMail, mail); rcpt != NULL; rcpt = next) {
        next = RB_NEXT(OutMail, mail, rcpt);
        RB_REMOVE(OutMail, mail, rcpt);
        MailEl* mail;
        LIST_FOREACH(mail, rcpt->mail, next) {
            int have_unsended = 0;
            StringEl *n2, *n1 = LIST_FIRST(&mail->info->recipients);
            while ( n1 != NULL ) {
                n2 = LIST_NEXT(n1, next);
                if ( n1 != NULL && n1->content[0] == '\0' ) {
                    LIST_REMOVE(n1, next);
                    free(n1->content);
                    free(n1);
                } else {
                    have_unsended = 1;
                }
                n1 = n2;
            }
            if ( have_unsended )
                marshall_mail(mail->info, mail->path);
            else if ( unlink(mail->path) )
                error("Can't unlink '%s': %s", mail->path, strerror(errno));
        }
    }

    return 0;
}

void* _process_domain(void* queue) {
#define _copy_str(to, from) {                           \
        to = malloc((strlen(from) + 1) * sizeof(char)); \
        strcpy(to, from);                               \
    }

    DomainMail* task;
    while ( (task = get_task(queue)) != NULL ) {
        info("Start '%s'", task->domain);
        char* mx_host = get_domain_mx(task->domain);
        if ( mx_host == NULL ) continue;

        int connection = 0;

        MailEl* mail;
        LIST_FOREACH(mail, task->mail, next) {
            if ( !connection ) {
                connection = connect_to_mx(mx_host, OPT_VALUE_PORT, OPT_VALUE_RETRIES, OPT_VALUE_INTERVAL, OPT_VALUE_TIMEOUT);
                if ( connection <= 0 || get_response_status(connection, NULL, OPT_VALUE_TIMEOUT).status != r220 ) {
                    warn("Can't reconnect to '%s'", mx_host);
                    connection = 0;
                }
            }
            if ( connection ) {
                Mail* copy = new_mail();
                _copy_str(copy->data, mail->info->data)
                _copy_str(copy->x_original_to, mail->info->x_original_to);
                _copy_str(copy->from, mail->info->from);
                StringEl* recp;
                LIST_FOREACH(recp, &mail->info->recipients, next) {
                    char* domain = get_email_domain(recp->content);
                    if ( !strcmp(domain, task->domain) ) {
                        StringEl* recp_copy = malloc(sizeof(StringEl));
                        _copy_str(recp_copy->content, recp->content);
                        LIST_INSERT_HEAD(&copy->recipients, recp_copy, next);
                    }
                    if ( domain ) free(domain);
                }

                if ( !run(connection, copy, OPT_VALUE_TIMEOUT) ) {
                    LIST_FOREACH(recp, &mail->info->recipients, next) {
                        char* domain = get_email_domain(recp->content);
                        if ( !strcmp(domain, task->domain) ) recp->content[0] = '\0';
                        if ( domain ) free(domain);
                    }
                } else {
                    close(connection);
                    connection = 0;
                }
                free_mail(&copy);
            }
        }
        free(mx_host);
        info("Done '%s'", task->domain);
    }
    return 0;
}
