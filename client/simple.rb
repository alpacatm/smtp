#!/usr/bin/ruby

require "mini-smtp-server"

class TestSmtpServer < MiniSmtpServer
  def new_message_event(message_hash)
    puts message_hash
  end
end

server = TestSmtpServer.new(2525, "127.0.0.1", 4)
server.audit = true

# Start the server
server.start

sleep