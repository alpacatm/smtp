#pragma once

#include "sys/queue.h"
#include "pthread.h"

typedef struct TaskEl {
    void* content;
    TAILQ_ENTRY(TaskEl) next;
} TaskEl;

typedef TAILQ_HEAD(,TaskEl) TaskHead;
typedef struct TaskQueue {
    pthread_mutex_t lock;
    pthread_cond_t has_some;
    TaskHead* tasks;
} TaskQueue;

TaskQueue* new_queue();

void add_task(TaskQueue* queue, void* task);

void* get_task(TaskQueue* queue);