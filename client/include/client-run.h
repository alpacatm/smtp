#pragma once

/** \file client-run.h
*  \brief Основные функции.
*
*   В этом файле описана главная функция программы.
*/

#include "mail.h"

/**
 * Структура, полностью отображающее попытку перехода
 */
typedef struct response_status {
    int status;    /**< результат переход */
    int smtp_code; /**< smtp код результата */
} response_status;

/** \fn int run(int connection, Mail* mail, size_t timeout)
*  \brief Функция, посылающая mx серверу письмо.
*  \param connection -- соединение с сервером.
*  \param mail -- посылаемое письмо.
*  \param timeout -- таймауты на запись/чтение.
*  \return 0 в случае успеха, иначе код ошибки
*/
int run(int connection, Mail* mail, size_t timeout);

/** \fn response_status get_response_status(int connection, const char* cmd, size_t timeout)
*  \brief Функция, посылающая команду mx серверу и обрабатывающая ответ сервера на неё.
*  \param connection -- соединение с сервером.
*  \param cmd -- посылаемая команда.
*  \param timeout -- таймауты на запись/чтение.
*  \return статус возврата
*/
response_status get_response_status(int connection, const char* cmd, size_t timeout);
