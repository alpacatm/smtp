#pragma once

/** \file client-cmd.h
 *  \brief Функции обработки команд и изменения состояния сервера.
 *
 *   В этом файле описано состояние сервера, а также вызываемые
 *   из конечного автомата функции обработки команд.
 */

#include "mx-state.h"
#include "mail.h"

/** \fn te_client_fsm_state client_cmd_ehlo(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая команду EHLO и обрабатывающая ответ сервера на неё.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_CMD_MAIL в случае успеха, в CLIENT_FSM_EV_CMD_ERROR или CLIENT_FSM_EV_TIMEOUT в случае ошибок.
*  \return CLIENT_FSM_ST_EHLO в случае успеха, иначе текущее состояние
*/
te_client_fsm_state client_cmd_ehlo(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_mail(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая команду MAIL и обрабатывающая ответ сервера на неё.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_CMD_RCPT в случае успеха, в CLIENT_FSM_EV_CMD_ERROR или CLIENT_FSM_EV_TIMEOUT в случае ошибок.
*  \return CLIENT_FSM_ST_MAIL в случае успеха, иначе текущее состояние
*/
te_client_fsm_state client_cmd_mail(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_init_rcpt(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, подготавливающая автомат к отправке получателей.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_CMD_RCPT, в CLIENT_FSM_EV_CMD_ERROR или CLIENT_FSM_EV_TIMEOUT в случае ошибок.
*  \return CLIENT_FSM_ST_RCPT.
*/
te_client_fsm_state client_cmd_init_rcpt(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_yar(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая команду RCPT и обрабатывающая ответ сервера на неё, если ещё есть неотправленные получатели.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, в случае успеха устанавливается в CLIENT_FSM_EV_CMD_RCPT если есть ещё неотправленные получатели, иначе в CLIENT_FSM_EV_CMD_DATA, и в CLIENT_FSM_EV_CMD_ERROR или CLIENT_FSM_EV_TIMEOUT в случае ошибок.
*  \return CLIENT_FSM_ST_RCPT в случае успеха, иначе текущее состояние
*/
te_client_fsm_state client_cmd_yar(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_init_data(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая команду EHLO и обрабатывающая ответ сервера на неё.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_CMD_MAIL в случае успеха, в CLIENT_FSM_EV_CMD_ERROR или CLIENT_FSM_EV_TIMEOUT в случае ошибок.
*  \return CLIENT_FSM_ST_EHLO в случае успеха, иначе текущее состояние.
*/
te_client_fsm_state client_cmd_init_data(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_data(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая команду DATA и обрабатывающая ответ сервера на неё.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_CMD_DATA в случае успеха, в CLIENT_FSM_EV_CMD_ERROR или CLIENT_FSM_EV_TIMEOUT в случае ошибок.
*  \return CLIENT_FSM_ST_DATA в случае успеха, иначе текущее состояние.
*/
te_client_fsm_state client_cmd_data(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_error(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая текст письма и обрабатывающая ответ сервера на него.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_CMD_ERROR.
*  \return CLIENT_FSM_ST_DONE.
*/
te_client_fsm_state client_cmd_error(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);

/** \fn te_client_fsm_state client_cmd_timeout(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev)
*  \brief Функция, посылающая команду EHLO и обрабатывающая ответ сервера на неё.
*  \param connection -- соединение с сервером;
*  \param mail -- письмо, обрабатываемое в данный момент.
*  \param state -- состояние сервера.
*  \param timeout -- таймауты на запись/чтение.
*  \param ev -- следующий переход, устанавливается в CLIENT_FSM_EV_TIMEOUT.
*  \return CLIENT_FSM_ST_DONE.
*/
te_client_fsm_state client_cmd_timeout(int connection, Mail* mail, mx_state* state, size_t timeout, te_client_fsm_event* ev);
