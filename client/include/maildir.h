#pragma once

#include <sys/queue.h>
#include <bsd/sys/tree.h>
#include "string.h"
#include "mail.h"

typedef struct MailEl {
    const Mail* info;
    const char* path;
    LIST_ENTRY(MailEl) next;
} MailEl;

typedef LIST_HEAD(,MailEl) MailsHead;

typedef struct DomainMail {
    char* domain;
    MailsHead* mail;
    RB_ENTRY(DomainMail) entry;
} DomainMail;

typedef RB_HEAD(OutMail, DomainMail) OutMail;
int _cmp_domains(const DomainMail *a, const DomainMail *b);
RB_PROTOTYPE(OutMail, DomainMail, entry, _cmp_domains)

OutMail* get_maildir_transfer(const char* dir);