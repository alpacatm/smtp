#pragma once
/** \file mx-state.h
 *  В этом файле описана структура с состоянием и фунции по её инициализации и освобождению.
 */

#include "client-fsm.h"
#include "mail.h"

/**
 *  \brief Структура, хранящее дополниетельную информацию о состоянии.
 *
 */
typedef struct mx_state {
    te_client_fsm_state state;
    StringEl* rcpt;
} mx_state;

void init_state(mx_state *state);

void free_state(mx_state *state);
